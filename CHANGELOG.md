# Changelog information

## v.0.5.2

----

### New features:

- Added `convert_mole_to_mass_fractions` and `convert_mass_to_mole_fractions` functions
- Added `cmp_f` and `mxt_f` functions for calculation of Helmholtz free energy with respect to given reference state

----

### Bug fixes:

- Corrected the bug where mixture component initialized with first record empty causing some runtime errors
- Corrected error messages that provided wrong function name reference when error was encountered in `find_cmp_sig_coeffs`
- Corrected error where after unhandled error is encountered `CurDir` property was reset to default
- Corrected the bug where critical volume property function `cmp_vc` return value was 1000x bigger that expected
- Corrected the bug where enthalpy of formation and reference state entropy were calculated with respect to standard temperature and not reference temperature

-----

### Performance improvements:

- Improved performance of `find_cmp_sig_coeffs` and `find_cmp_rg_coeffs` by limiting number of times both of these functions accessed data base per single run
- Separated `find_cmp_sig_coeffs` and `find_cmp_rg_coeffs` from code and workbook access. Code access should be performed now by calling `get_cmp_sig_coeffs` and `get_cmp_rg_coeffs`
- Moved program settings to `thermoPropSettings.json` from workbook storage. External file improves the stability of the TP and accessibility to the TP settings
- Removed unnecessary defined global constants `EXT_VERSION` and `EXT_NAME`. Stability will improve especially during unintentional changes to the TP file name
- Added `clsComponent` and `clsMixture` classes interfaces that create objects desired for the purpose either of workbook calling functions or external API

-----

## v.0.5.1

----

### New features:

- This is the first public release of the ThermoProp  Toolpack see [documentation](./ThermoProp_Toolpack_v.0.5.1_documentation.pdf) for features list and description

----