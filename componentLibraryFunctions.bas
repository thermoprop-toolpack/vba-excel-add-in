Attribute VB_Name = "componentLibraryFunctions"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.Utilities")
''-----------------------------------------------------------------------------------------------''

Option Explicit
Option Private Module

''Module is used to find coefficients of the SIG based on NASA Glenn research center 9 coefficient library
''and RG parameters i.e. Tc, pc, accentricity coefficient as well as molar mass based on R. C. Reid,
''J. M. Prausnitz, and B. E. Poling, 1987, "The Properties of Gases and Liquids, 4th Ed."
''Some utility functions returning library of available species are also available

Public Function get_cmp_sig_coeffs(ByVal cmp_name As String, ByVal cmp_T As Double, _
                                   Optional ByVal cmp_phase As String = "gas") As Variant()
                                    
#If Debugging Then
    Dim perfCounter As clsPerformanceCounter
    Set perfCounter = New clsPerformanceCounter
    perfCounter.StartCounter
    
#End If
                                    
    ''Finds and returns component semi-ideal gas coefficients
    Dim ws As Worksheet
    Dim rg_nm As Range, rg_cf As Range
    Dim i As Integer
    Dim cmp_ID As Integer
    
    On Error GoTo err_gate
    
    Set ws = ThisWorkbook.Worksheets("tbCoeffs")
    With ws
        Set rg_nm = .Range("tbCoeffs_cmpList")
        Set rg_cf = .Range("tbCoeffs_cmpACoeffs", "tbCoeffs_cmpBCoeffs")
        
    End With
    
    cmp_ID = -3
    
    Dim cmp_names() As Variant
    ReDim cmp_names(1 To rg_nm.Rows.Count, 1 To rg_nm.Columns.Count)
    cmp_names = rg_nm
    
    For i = LBound(cmp_names, 1) To UBound(cmp_names, 1)
        If cmp_names(i, 1) = cmp_name Then
            cmp_ID = -2
            If cmp_names(i, 2) = cmp_phase Then
                cmp_ID = -1
                If cmp_names(i, 3) <= cmp_T And cmp_names(i, 4) > cmp_T Then
                    cmp_ID = i
                    Exit For
                    
                End If
                
            End If
            
        End If
    
    Next i
    
    Select Case cmp_ID
        Case -3
            Err.Raise number:=vbObjectError + ERR_WRONG_CMP, _
                  Source:="componentLibraryFunction.get_cmp_sig_coeffs", _
                  Description:="Failed to find component: " + cmp_name + " in reference library"
        
        Case -2
            Err.Raise number:=vbObjectError + ERR_WRONG_PHS, _
                  Source:="componentLibraryFunction.get_cmp_sig_coeffs", _
                  Description:="Semi-ideal gas coefficients for component: " + cmp_name + " for phase: " + cmp_phase + _
                               " have not been found"
                             
        Case -1
            Err.Raise number:=vbObjectError + ERR_WRONG_TMP, _
                  Source:="componentLibraryFunction.get_cmp_sig_coeffs", _
                  Description:="Semi-ideal gas coefficients for component: " + cmp_name + " in temperature of: " + Str(cmp_T) + _
                               "K have not been found - temperature is out of range"
        
    End Select
    
    get_cmp_sig_coeffs = rg_cf.Rows.Item(cmp_ID).value
    
#If Debugging Then
    Debug.Print perfCounter.TimeElapsed
    
#End If

normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "componentLibraryFunctions.get_cmp_sig_coeffs", Err.Description

End Function

Public Function get_cmp_rg_coeffs(ByVal cmp_name As String) As Variant()
                                                                       
#If Debugging Then
    Dim perfCounter As clsPerformanceCounter
    Set perfCounter = New clsPerformanceCounter
    perfCounter.StartCounter
    
#End If

    ''Finds and returns component critical coefficients and molar mass
    Dim ws As Worksheet
    Dim rg_nm As Range, rg_cf As Range
    Dim i As Integer
    Dim cmp_ID As Integer
    
    On Error GoTo err_gate
    
    Set ws = ThisWorkbook.Worksheets("tbRGCoeffs")
    With ws
        Set rg_nm = .Range("tbRGCoeffs_cmpList")
        Set rg_cf = .Range("tbRGCoeffs_cmpCritProps", "tbRGCoeffs_cmpOtherProps")
        
    End With
    
    Dim cmp_names() As Variant
    ReDim cmp_names(1 To rg_nm.Rows.Count, 1 To rg_nm.Columns.Count)
    cmp_names = rg_nm
    
    cmp_ID = -1
    
    For i = 1 To rg_nm.Rows.Count
        If cmp_names(i, 1) = cmp_name Then
            cmp_ID = i
            Exit For
            
        End If
        
    Next i
    
    Select Case cmp_ID
        Case -1
            Err.Raise number:=vbObjectError + ERR_WRONG_CMP, _
                  Source:="componentLibraryFunction.get_cmp_rg_coeffs", _
                  Description:="Failed to find component: " + cmp_name + " in reference library"
        
    End Select
    
    get_cmp_rg_coeffs = rg_cf.Rows.Item(cmp_ID).value
    
#If Debugging Then
    Debug.Print perfCounter.TimeElapsed
    
#End If

normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "componentLibraryFunction.get_cmp_rg_coeffs", Err.Description

End Function

Public Function get_cmp_list() As Variant()
Attribute get_cmp_list.VB_Description = "Fetches list of available components. Returns array."
Attribute get_cmp_list.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Returns list of all components defined in the tool as an array
    Dim name_list_rg As Range
    
    Set name_list_rg = ThisWorkbook.Worksheets("tbRGCoeffs").Range("tbRGCoeffs_cmpList")

    get_cmp_list = name_list_rg.Offset(1, 0).Resize(name_list_rg.Rows.Count - 1).value
    
End Function

Public Function get_func_list() As Variant()
Attribute get_func_list.VB_Description = "Fetches list of defined functions. Returns array."
Attribute get_func_list.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Returns list of all functions defined in the tool as an array
    Dim func_list_rg As Range
    
    Set func_list_rg = ThisWorkbook.Worksheets("tbFunctionsList").Range("tbFunctionsList")

    get_func_list = func_list_rg.Offset(1, 1).Resize(func_list_rg.Rows.Count - 1, 1).value

End Function
