Attribute VB_Name = "ribbonMacros"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.UserInterphaseElements")
''-----------------------------------------------------------------------------------------------''

''TODO: Add following buttons to xml file: errors counter and warnings counter.

Option Explicit

Dim REF_STATES_COUNT As Integer

Private Sub utProgramInfo_OnAction(ByRef control As Office.IRibbonControl)
    UF_welcome_message.Show

End Sub

Private Sub utHelp_OnAction(ByRef control As Office.IRibbonControl)
    UF_help_prompt.Show

End Sub

Private Sub utWarningLog_OnAction(ByRef control As Office.IRibbonControl)
    Dim log_addr As String
    log_addr = CurDir + Right(RNT_LOG_PATH, Len(RNT_LOG_PATH) - 1)
    CreateObject("Shell.Application").Open (log_addr)
    
End Sub

Private Sub utErrorLog_OnAction(ByRef control As Office.IRibbonControl)
    Dim log_addr As String
    log_addr = CurDir + Right(ERR_LOG_PATH, Len(ERR_LOG_PATH) - 1)
    CreateObject("Shell.Application").Open (log_addr)

End Sub

Private Sub unParticles_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_N = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unParticles_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_N <> vbNullString Then
        itemID = "pr" + UNIT_N
    
    Else
        itemID = "prmol"
        UNIT_N = Mid(itemID, 3, Len(itemID) - 2)
        
    End If

End Sub

Private Sub unVolume_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_V = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unVolume_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_V <> vbNullString Then
        itemID = "vl" + UNIT_V
        
    Else
        itemID = "vlm3"
        UNIT_V = Mid(itemID, 3, Len(itemID) - 2)
    
    End If
    
End Sub

Private Sub unMass_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_M = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unMass_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_M <> vbNullString Then
        itemID = "ms" + UNIT_M
        
    Else
        itemID = "mskg"
        UNIT_M = Mid(itemID, 3, Len(itemID) - 2)
    
    End If
    
End Sub

Private Sub unEnergy_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_E = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unEnergy_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_E <> vbNullString Then
        itemID = "en" + UNIT_E
    
    Else
        itemID = "enJ"
        UNIT_E = Mid(itemID, 3, Len(itemID) - 2)
    
    End If

End Sub

Private Sub unPressure_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_P = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unPressure_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_P <> vbNullString Then
        itemID = "pr" + UNIT_P
        
    Else
        itemID = "prPa"
        UNIT_P = Mid(itemID, 3, Len(itemID) - 2)

    End If

End Sub

Private Sub unTemperature_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_T = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub unTemperature_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_T <> vbNullString Then
        itemID = "tp" + UNIT_T
        
    Else
        itemID = "tpK"
        UNIT_T = Mid(itemID, 3, Len(itemID) - 2)
    
    End If

End Sub

Private Sub ddBase_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    UNIT_BASE = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull

End Sub

Private Sub ddBase_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If UNIT_BASE <> vbNullString Then
        itemID = "bs" + UNIT_BASE
    
    Else
        itemID = "bsmole"
        UNIT_BASE = Mid(itemID, 3, Len(itemID) - 2)

    End If

End Sub

Private Sub ddReferenceConditions_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    Select Case Mid(id, 3, Len(id) - 2)
        Case "STP"
            REF_P = 100000
            REF_T = 273.15
            
        Case "ISO"
            REF_P = 101325
            REF_T = 288.15
            
        Case "STD"
            REF_P = 101325
            REF_T = 298.15
            
        Case "NIST"
            REF_P = 101325
            REF_T = 293.15
            
        Case "UD"
            ''UD label has to be changed to button so that it can be triggered on every selection
            UF_UD_reference_conditions.Show
    
    End Select
    
    Application.CalculateFull

End Sub

Private Sub ddReferenceConditions_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If REF_P <> 0 And REF_T <> 0 Then
        Select Case REF_P
            Case 100000
                Select Case REF_T
                    Case 273.15
                        itemID = "rcSTP"
                        
                    Case Else
                        itemID = "rcUD"
                        
                End Select
                
            Case 101325
                Select Case REF_T
                    Case 288.15
                        itemID = "rcISO"
                    
                    Case 298.15
                        itemID = "rcSTD"
                    
                    Case 293.15
                        itemID = "rcNIST"
                    
                    Case Else
                        itemID = "rcUD"
                        
                End Select
            
            Case Else
                itemID = "rcUD"
            
        End Select
        
    Else
        itemID = "rcSTP"
        REF_P = 100000
        REF_T = 273.15
        
    End If
    
End Sub

Private Sub ddThermoModel_OnAction(ByRef control As Office.IRibbonControl, ByRef id As String, ByRef index As Variant)
    THERMO_MODEL = Mid(id, 3, Len(id) - 2)
    Application.CalculateFull
    
End Sub

Private Sub ddThermoModel_OnGetSelectedItemID(ByRef control As IRibbonControl, ByRef itemID As Variant)
    If THERMO_MODEL <> vbNullString Then
        itemID = "tm" + THERMO_MODEL
        
    Else
        itemID = "tmPR"
        THERMO_MODEL = Mid(itemID, 3, Len(itemID) - 2)

    End If
    
End Sub
