Attribute VB_Name = "initializationProcedures"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.InitializationAndConstants")
''-----------------------------------------------------------------------------------------------''

Option Explicit
Option Private Module

Public Sub extension_initialize()
    ''Sub called to initialize TP correctly
    ''Call to function help initializer making tooltips available in function wizard
    On Error Resume Next ''This is to avoid error if extension gets loaded on it's own w/o new sheet with it.
    functionHelpPromptGenerator.define_function
    On Error GoTo 0
                             
    set_working_directory
                             
    IOModule.load_settings_file_to_dict
    settings_dict_consistency_check
    initialize_global_units
    
    welcome_message_check
    
    initialize_log_files
    
End Sub

Public Sub extension_close()
    ''Sub called to close TP correctly
    Dim save_globals_flag As Integer
    set_working_directory
    
    save_globals_flag = MsgBox("Save current unit set before closing?", _
                               vbYesNo + vbQuestion, "Save ThermoProp state")
                               
    If save_globals_flag = vbYes Then
        update_dict_with_globals
        IOModule.save_settings_dict_to_file
        
    End If
    
    close_runtime_log_file
    restore_working_directory
    
End Sub

Public Sub settings_dict_consistency_check()
    ''Sub checks whether SETTINGS_DICT is of correct structure so that globals are initialized properly
    Dim buff_dict As Dictionary
    Set buff_dict = New Dictionary
    buff_dict.Add "thermoModel", "PR"
    buff_dict.Add "refP", 100000#
    buff_dict.Add "refT", 273.15
    buff_dict.Add "unitBase", "mole"

    If TypeName(SETTINGS_DICT) = "Nothing" Then
        Set SETTINGS_DICT = New Dictionary
        
    End If

    If SETTINGS_DICT.Exists("tpGeneral") = False Then
        SETTINGS_DICT.Add "tpGeneral", buff_dict
        
    Else
        If is_dictionary(SETTINGS_DICT("tpGeneral")) = False Then
            SETTINGS_DICT.Remove "tpGeneral"
            SETTINGS_DICT.Add "tpGeneral", buff_dict
        
        ElseIf SETTINGS_DICT("tpGeneral").Exists("thermoModel") = False Then
            SETTINGS_DICT("tpGeneral").Add "thermoModel", buff_dict("thermoModel")
        
        ElseIf SETTINGS_DICT("tpGeneral").Exists("refP") = False Then
            SETTINGS_DICT("tpGeneral").Add "refP", buff_dict("refP")
        
        ElseIf SETTINGS_DICT("tpGeneral").Exists("refT") = False Then
            SETTINGS_DICT("tpGeneral").Add "refT", buff_dict("refT")
        
        ElseIf SETTINGS_DICT("tpGeneral").Exists("unitBase") = False Then
            SETTINGS_DICT("tpGeneral").Add "unitBase", buff_dict("unitBase")
        
        End If
        
    End If
    
    Set buff_dict = New Dictionary
    buff_dict.Add "unitT", "K"
    buff_dict.Add "unitP", "Pa"
    buff_dict.Add "unitE", "J"
    buff_dict.Add "unitM", "kg"
    buff_dict.Add "unitV", "m3"
    buff_dict.Add "unitN", "mol"
    
    If SETTINGS_DICT.Exists("tpUnits") = False Then
        SETTINGS_DICT.Add "tpUnits", buff_dict
    
    Else
        If is_dictionary(SETTINGS_DICT("tpUnits")) = False Then
            SETTINGS_DICT.Remove "tpUnits"
            SETTINGS_DICT.Add "tpUnits", buff_dict
        
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitT") = False Then
            SETTINGS_DICT("tpUnits").Add "unitT", buff_dict("unitT")
            
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitP") = False Then
            SETTINGS_DICT("tpUnits").Add "unitP", buff_dict("unitP")
        
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitE") = False Then
            SETTINGS_DICT("tpUnits").Add "unitE", buff_dict("unitE")
        
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitM") = False Then
            SETTINGS_DICT("tpUnits").Add "unitM", buff_dict("unitM")
        
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitV") = False Then
            SETTINGS_DICT("tpUnits").Add "unitV", buff_dict("unitV")
        
        ElseIf SETTINGS_DICT("tpUnits").Exists("unitN") = False Then
            SETTINGS_DICT("tpUnits").Add "unitN", buff_dict("unitN")
        
        End If
    
    End If
    
    Set buff_dict = New Dictionary
    buff_dict.Add "extPath", ThisWorkbook.Path
    buff_dict.Add "logPath", ".\logs"
    buff_dict.Add "errLogPath", ".\logs\error.log"
    buff_dict.Add "rntLogPath", ".\logs\runtime.log"
    buff_dict.Add "dbgLogPath", ".\logs\debug.log"
    
    If SETTINGS_DICT.Exists("tpUtilities") = False Then
        SETTINGS_DICT.Add "tpUtilities", buff_dict
        
    Else
        If is_dictionary(SETTINGS_DICT("tpUtilities")) = False Then
            SETTINGS_DICT.Remove "tpUtilities"
            SETTINGS_DICT.Add "tpUtilities", buff_dict
        
        ElseIf SETTINGS_DICT("tpUtilities").Exists("extPath") = False Then
            SETTINGS_DICT("tpUtilities").Add "extPath", buff_dict("extPath")
        
        ElseIf SETTINGS_DICT("tpUtilities").Exists("logPath") = False Then
            SETTINGS_DICT("tpUtilities").Add "logPath", buff_dict("logPath")
        
        ElseIf SETTINGS_DICT("tpUtilities").Exists("errLogPath") = False Then
            SETTINGS_DICT("tpUtilities").Add "errLogPath", buff_dict("errLogPath")
        
        ElseIf SETTINGS_DICT("tpUtilities").Exists("rntLogPath") = False Then
            SETTINGS_DICT("tpUtilities").Add "rntLogPath", buff_dict("rntLogPath")
        
        ElseIf SETTINGS_DICT("tpUtilities").Exists("dbgLogPath") = False Then
            SETTINGS_DICT("tpUtilities").Add "dbgLogPath", buff_dict("dbgLogPath")
        
        End If
    
    End If
    
    Set buff_dict = New Dictionary
    buff_dict.Add "showWelcomeMessage", True
    
    If SETTINGS_DICT.Exists("tpFlags") = False Then
        SETTINGS_DICT.Add "tpFlags", buff_dict
    
    Else
        If is_dictionary(SETTINGS_DICT("tpFlags")) = False Then
            SETTINGS_DICT.Remove "tpFlags"
            SETTINGS_DICT.Add "tpFlags", buff_dict
        
        ElseIf SETTINGS_DICT("tpFlags").Exists("showWelcomeMessage") = False Then
            SETTINGS_DICT("tpFlags").Add "showWelcomeMessage", buff_dict("showWelcomeMessage")
        
        End If
    
    End If

End Sub

Public Function is_dictionary(ByRef dict_candidate As Variant) As Boolean
    ''Function checks whether argument passed is dictionary
    If TypeName(dict_candidate) = "Dictionary" Then
        is_dictionary = True
        
    Else
        is_dictionary = False
        
    End If

End Function

Public Sub initialize_global_units()
    ''Function extracts values from SETTINGS_DICT to respective global units
    Dim querry_answer As Integer
    
    'NOTE: This has to be done with dedicated UF that will have a "don't ask again" flag
    querry_answer = MsgBox(Prompt:="Would you like to load settings from settings file (Yes)" & _
                                    " or use default ones (No)?", _
                    Buttons:=vbYesNo + vbQuestion, Title:="Load settings")
                    
    If querry_answer = vbNo Then
        Set SETTINGS_DICT = Nothing
        settings_dict_consistency_check
        
    End If
    
    assign_globals_from_dict
    
End Sub

Public Sub assign_globals_from_dict()
    ''This function assigns globals from dictionary buffer
    ''It is separated from initialize_global_units since it is also used during execution consistency check
    THERMO_MODEL = SETTINGS_DICT("tpGeneral")("thermoModel")
    REF_P = SETTINGS_DICT("tpGeneral")("refP")
    REF_T = SETTINGS_DICT("tpGeneral")("refT")
    UNIT_BASE = SETTINGS_DICT("tpGeneral")("unitBase")
    
    UNIT_T = SETTINGS_DICT("tpUnits")("unitT")
    UNIT_P = SETTINGS_DICT("tpUnits")("unitP")
    UNIT_E = SETTINGS_DICT("tpUnits")("unitE")
    UNIT_M = SETTINGS_DICT("tpUnits")("unitM")
    UNIT_V = SETTINGS_DICT("tpUnits")("unitV")
    UNIT_N = SETTINGS_DICT("tpUnits")("unitN")
    
    EXT_PATH = SETTINGS_DICT("tpUtilities")("extPath")
    LOG_PATH = SETTINGS_DICT("tpUtilities")("logPath")
    ERR_LOG_PATH = SETTINGS_DICT("tpUtilities")("errLogPath")
    RNT_LOG_PATH = SETTINGS_DICT("tpUtilities")("rntLogPath")
    DBG_LOG_PATH = SETTINGS_DICT("tpUtilities")("dbgLogPath")

    WLC_MSG_FLAG = SETTINGS_DICT("tpFlags")("showWelcomeMessage")

End Sub

Public Sub update_dict_with_globals()
    ''Sub updates settings dictionary with current set of globals
    SETTINGS_DICT("tpGeneral")("thermoModel") = THERMO_MODEL
    SETTINGS_DICT("tpGeneral")("refP") = REF_P
    SETTINGS_DICT("tpGeneral")("refT") = REF_T
    SETTINGS_DICT("tpGeneral")("unitBase") = UNIT_BASE
    
    SETTINGS_DICT("tpUnits")("unitT") = UNIT_T
    SETTINGS_DICT("tpUnits")("unitP") = UNIT_P
    SETTINGS_DICT("tpUnits")("unitE") = UNIT_E
    SETTINGS_DICT("tpUnits")("unitM") = UNIT_M
    SETTINGS_DICT("tpUnits")("unitV") = UNIT_V
    SETTINGS_DICT("tpUnits")("unitN") = UNIT_N
    
    SETTINGS_DICT("tpUtilities")("extPath") = EXT_PATH
    SETTINGS_DICT("tpUtilities")("logPath") = LOG_PATH
    SETTINGS_DICT("tpUtilities")("errLogPath") = ERR_LOG_PATH
    SETTINGS_DICT("tpUtilities")("rntLogPath") = RNT_LOG_PATH
    SETTINGS_DICT("tpUtilities")("dbgLogPath") = DBG_LOG_PATH

    SETTINGS_DICT("tpFlags")("showWelcomeMessage") = WLC_MSG_FLAG

End Sub

Public Sub set_working_directory()
    ''Sub sets current working directory to TPs dir so that logger works correctly
    EXT_PATH = ThisWorkbook.Path
    BUFF_PATH = CurDir
    If CurDir <> EXT_PATH Then
        ChDir EXT_PATH
    
    End If

End Sub

Public Sub restore_working_directory()
    ''Sub restores working directory to original one prior TP was launched
    If CurDir <> BUFF_PATH Then
        ChDir BUFF_PATH
        
    End If

End Sub

Public Sub welcome_message_check()
    ''Sub checks for welcome message flag and if it is set to True, shows welcome message
    If WLC_MSG_FLAG = True Then
        UF_welcome_message.Show
    
    End If

End Sub

Public Sub initialize_log_files()
    '' Creates or initializes log files depending on their status in file system
    Dim fso As New FileSystemObject
    Dim old_err_log_path As String
    Dim procedure_location As String
    
    procedure_location = "globalVariables.initialize_log_files"
    
    old_err_log_path = LOG_PATH + "\old_err.log"
    If fso.FolderExists(LOG_PATH) = False Then
        fso.CreateFolder (LOG_PATH)
        
    End If
    
    If fso.FileExists(ERR_LOG_PATH) = False Then
        fso.CreateTextFile ERR_LOG_PATH
        logger log_file_path:=ERR_LOG_PATH, _
               info_type:="Information", _
               message:="Error log file has been created", _
               procedure:=procedure_location
        
    Else
        If fso.GetFile(ERR_LOG_PATH).Size = 0 Then
            logger log_file_path:=ERR_LOG_PATH, _
                   info_type:="Information", _
                   message:="Error log file has been created", _
                   procedure:=procedure_location
            
        ElseIf fso.GetFile(ERR_LOG_PATH).Size > 20971520 Then
            fso.CopyFile Source:=ERR_LOG_PATH, Destination:=old_err_log_path, OverWriteFiles:=True
            fso.DeleteFile FileSpec:=ERR_LOG_PATH
            fso.CreateTextFile ERR_LOG_PATH
            logger log_file_path:=ERR_LOG_PATH, _
                   info_type:="Information", _
                   message:="Size of the log file too large, old log archived under old_err.log name", _
                   procedure:=procedure_location
        
        End If
        
    End If
    
    If fso.FileExists(RNT_LOG_PATH) = False Then
        fso.CreateTextFile RNT_LOG_PATH
        logger log_file_path:=RNT_LOG_PATH, _
               info_type:="Information", _
               message:="Runtime log file has been created", _
               procedure:=procedure_location
        
    Else
        If fso.GetFile(RNT_LOG_PATH).Size = 0 Then
            logger log_file_path:=RNT_LOG_PATH, _
                   info_type:="Information", _
                   message:="Runtime log file has been created", _
                   procedure:=procedure_location
            
        End If
        
    End If
    
    ''Currently this piece is not needed, it'll have a use while testing is implemented
    #If Debbugging Then
        If fso.FileExists(DBG_LOG_PATH) = False Then
            fso.CreateTextFile DBG_LOG_PATH
            logger log_file_path:=DBG_LOG_PATH, _
                   info_type:="Information", _
                   message:="Debug log file has been created", _
                   procedure:=procedure_location

        Else
            If fso.GetFile(DBG_LOG_PATH).Size = 0 Then
                logger log_file_path:=DBG_LOG_PATH, _
                       info_type:="Information", _
                       message:="Debug log file has been created", _
                       procedure:=procedure_location

                
            End If
        
        End If
        
    #End If

End Sub

Public Sub close_runtime_log_file()
    ''Function dumps runtime log into separate file available for later review
    Dim fso As New FileSystemObject
    Dim old_rnt_log_path As String

    old_rnt_log_path = LOG_PATH + "\old_rnt.log"

    fso.CopyFile Source:=RNT_LOG_PATH, Destination:=old_rnt_log_path, OverWriteFiles:=True
    fso.DeleteFile FileSpec:=RNT_LOG_PATH

End Sub

