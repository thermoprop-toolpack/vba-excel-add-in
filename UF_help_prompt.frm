VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UF_help_prompt 
   Caption         =   "ThermoProp Toolpack Help"
   ClientHeight    =   5415
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5760
   OleObjectBlob   =   "UF_help_prompt.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UF_help_prompt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'@Folder("ThermoPropPack.UserInterphaseElements")
Option Explicit

Private Sub UserForm_Initialize()
    Me.Top = Application.Top + (Application.UsableHeight / 2) - (Me.Height / 2)
    Me.Left = Application.Left + (Application.UsableWidth / 2) - (Me.Width / 2)
    
    Dim num_functions As Long
    Dim i As Long
    Dim buff As Variant
    
    With tbFunctionList.Range("tbFunctionsList")
        num_functions = .Rows.Count - 1
        buff = .Resize(num_functions, 1).Offset(1, 1).value
    
    End With
    
    CbB_functionNames.AddItem vbNullString
    
    For i = 1 To num_functions
        With CbB_functionNames
            .AddItem buff(i, 1)
        
        End With
        
    Next i
    
    With tb_helpPrompt
        .BorderColor = &H80000006
        .BorderStyle = fmBorderStyleSingle
        .AutoSize = False
        .MultiLine = True
        .WordWrap = True
        .Locked = True
        .ScrollBars = fmScrollBarsVertical
        .BackColor = &H8000000F
        .value = "Select function name from the combobox above. Help prompt will appear in this window."
        
    End With
    

End Sub

Private Sub CbB_functionNames_Change()
    Dim funcID As Long
    Dim buff As Variant
    Dim input_str As String
    Dim input_vec As Variant
    Dim input_desc As Variant
    Dim i As Integer
    Dim lb_height As Double
    
    funcID = CbB_functionNames.ListIndex
    
    If funcID = 0 Then
       tb_helpPrompt.value = "Select function name from the combobox above. Help prompt will appear in this window."
    
    Else
        buff = tbFunctionList.Range("tbFunctionsList").Resize(1).Offset(funcID, 0).value
        input_str = vbNullString
        
        input_vec = Split(buff(1, 3), ";")
        input_desc = Split(buff(1, 4), ";")
        
        For i = 0 To (UBound(input_vec) - LBound(input_vec))
            input_str = input_str & input_vec(i) & vbNewLine & vbTab & input_desc(i) & vbNewLine
        
        Next i
        
        tb_helpPrompt.value = "Function belongs to " & buff(1, 1) & " group." & vbNewLine _
                                & "========================================" & vbNewLine _
                                & "Function name:" & vbNewLine _
                                & buff(1, 2) & vbNewLine _
                                & "========================================" & vbNewLine _
                                & "Function input args:" & vbNewLine _
                                & input_str & vbNewLine _
                                & "========================================" & vbNewLine _
                                & "Function output args:" & vbNewLine _
                                & buff(1, 5) & vbNewLine _
                                & "========================================" & vbNewLine _
                                & "Detailed description: " & vbNewLine _
                                & buff(1, 6)
                                
    End If

End Sub

Private Sub UserForm_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_help_prompt
    
    End If

End Sub

Private Sub CbB_functionNames_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_help_prompt
    
    End If

End Sub

Private Sub lb_funcName_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_help_prompt
    
    End If

End Sub

Private Sub tb_helpPrompt_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_help_prompt
    
    End If

End Sub

Private Sub Button_close_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_help_prompt
    
    End If

End Sub

Private Sub Button_close_Click()
    Unload UF_help_prompt

End Sub
