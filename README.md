# ThermoProp Toolpack v.0.5.1

## Introduction

ThermoProp Toolpack is dedicated Microsoft Excel add-in library that can be used to estimate thermodynamic properties of pure substances and their mixtures. TP currently has over 20 defined species like components of air, nobel gases, water, hydrocarbons contained in natural gas. For all of these species and their mixtures properties can be fetched with predefined set of functions straight from the worksheet. Program has defined over 10 basic thermodynamic functions of pure components and the same amount of functions for mixtures. In addition it can return specific properties of the components such as molar masses, enthalpies of formation, critical point data, etc.

In the TP user can select its own set of units for pressure, temperature, energy, mass, volume and particle count and TP will perform the calculation with result returned in the selected set of units as well. At present stage TP uses Peng Robinson real gas model for properties calculation, in the future additional models will be added. Phase equilibrium calculation is not yet implemented so saturation region calculation should be approached carefully.

If you intend to write additional routine that will further automate your work you can do just that with adding this add-in as a reference in your project. Detailed method of linking the TP to your project is described in the [documentation](./ThermoProp_Toolpack_v.0.5.2_documentation.pdf).

All properties are based on NASA Glenn Coefficients for Calculating Thermodynamic Properties of Individual Species _(NASA/TP--2002-211556)_ and critical point properties are taken from across the literature.

## Feedback

If you encounter issues with the software,  would like to propose a feature or just have questions write to [thermoprop.toolpack@gmail.com](mailto:thermoprop.toolpack@gmail.com). You can also post issue in this repository.

## License

ThermoProp is developed and distributed under GNU General Public License, version 2. For a full license text checkout [LICENSE](./LICENSE.txt) file.

## Download

If you want ready to use file download [ThermoProp_Toolpack_v.0.5.2.xlam](./ThermoProp_Toolpack_v.0.5.2.xlam) and put it in the trusted location of your machine.

If you want source files with thermodynamic properties database please download source files and incorporate them in your own VBA code.

Please note that source contains some [RubberDuck](https://rubberduckvba.com/) specific code annotations that serve no VBA functionality.

To run this extension properly you will need Microsoft Excel 2016 or later or Excel 365 edition desktop application.  For the application to run correctly you also need scripting library -- scrrun.dll available on your machine.

## Installation

After download locate xlam file in location suitable to you, remember that it has to be added to trusted locations in Excel otherwise it is going to keep pestering you on every launch. Suggested default location for the file is _C:\Users\\**user_name**\AppData\Roaming\Microsoft\AddIns_.

On the first launch TP will create _logs_ directory in the location it was opened from and will fill it with, well.. log files. These are necessary for troubleshooting and bug tracking.

You can make the TP to start with each start of the MS Excel. To do that go to File->Options->Add-ins->Go there either browse the location you put the program in or select the TP from the list if you decided to put it in the suggested location.

## Usage

From now on everything is rather simple. With the launch of the TP you will see additional tab on the ribbon called *ThermoProp* there you can find all settings you can tinker with, open log files from there and open help prompt. This help button is especially important since it gives you the access to brief version of the documentation of each function, explaining what it does, what are the inputs and their types.

To calculate, e.g. enthalpy of the CO2 species in 1MPa and 300K just type in the cell, as you would with a normal function:

```vb
	=cmp_h(1000000,300,"CO2")
	=12845 [J/kg]
```

If you want to fetch property, say enthalpy of formation of water vapor just type:

```vb
	=cmp_hf("H2O")
	=-241826 [J/mol]
```

To get the full list of components and functions defined in the ThermoProp use following two functions respectively - they both return column vector so please make sure that no data is located below cell you call them in order to avoid an error:

```vb
	=get_cmp_list()
	=get_func_list()
```

