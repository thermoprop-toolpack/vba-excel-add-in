Attribute VB_Name = "globalVariables"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.InitializationAndConstants")
''-----------------------------------------------------------------------------------------------''

Option Explicit
Option Private Module

''Global variables that constitute how calling functions behave (what they return)

Global SETTINGS_DICT As Dictionary ''Globally stored settings dictionary

Global UNIT_T As String         ''Assigned temperature unit
Global UNIT_P As String         ''Assigned pressure unit
Global UNIT_E As String         ''Assigned energy unit
Global UNIT_M As String         ''Assigned mass unit
Global UNIT_V As String         ''Assigned volume unit
Global UNIT_N As String         ''Assigned mole unit
Global UNIT_BASE As String      ''Base for specific energy calculations
Global THERMO_MODEL As String   ''Selected EOS model
Global REF_P As Double          ''Reference pressure
Global REF_T As Double          ''Reference temperature

Global EXT_PATH As String       ''Extension path in the file system
Global BUFF_PATH As String      ''Buffer to default directory in which application opens
Global LOG_PATH As String       ''Log folder location
Global ERR_LOG_PATH As String   ''Runtime error catcher logs
Global DBG_LOG_PATH As String   ''Debug error catcher log (developer only)
Global RNT_LOG_PATH As String   ''Runtime log path (temp log that gets created and deleted after closing Excel)

Global WLC_MSG_FLAG As Boolean  ''Welcome message flag (whether or not it should be show)

''Constants required for program stability
                                            
Global Const STF_NAME As String = "thermoPropSettings.json"
                                            ''Settings file name

Global Const R As Double = 8.31451070707    ''Universal gas constant, J/molK
Global Const NA As Double = 6.02214076E+23  ''Avogadro constant, part/mol
Global Const k_b As Double = 1.380649E-23   ''Boltzman constant, J/K
Global Const T_std As Double = 298.15       ''Standard temperature, K
Global Const p_std As Double = 100000#      ''Standard pressure, Pa



Public Sub global_unit_checker()
    ''TODO: Make it so that while user changes settings they got saved and in case of error this function loads saved values
    ''Checks if all variables are set correctly and if not reverses state to default one:
    ''THERMO_MODEL = "PR"
    ''UNIT_T = "K"
    ''UNIT_P = "Pa"
    ''UNIT_E = "J"
    ''UNIT_M = "kg"
    ''UNIT_V = "m3"
    ''UNIT_N = "mol"
    ''REF_P = 100000#
    ''REF_T = 273.15
    ''UNIT_BASE = "mole"
    Dim status_flag As Integer
    status_flag = 0
    
    If THERMO_MODEL = vbNullString Then THERMO_MODEL = "PR": status_flag = 1
    If UNIT_T = vbNullString Then UNIT_T = "K": status_flag = 1
    If UNIT_P = vbNullString Then UNIT_P = "Pa": status_flag = 1
    If UNIT_E = vbNullString Then UNIT_E = "J": status_flag = 1
    If UNIT_M = vbNullString Then UNIT_M = "kg": status_flag = 1
    If UNIT_V = vbNullString Then UNIT_V = "m3": status_flag = 1
    If UNIT_N = vbNullString Then UNIT_N = "mol": status_flag = 1
    If REF_P = 0 Then REF_P = 100000#: status_flag = 1
    If REF_T = 0 Then REF_T = 273.15: status_flag = 1
    If UNIT_BASE = vbNullString Then UNIT_BASE = "mole": status_flag = 1
    
    If status_flag = 1 Then
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_GLBVR_RST, _
               message:="Unit variables got lost and reset to default, set user default variables again.", _
               procedure:="callingFuncitons.global_unit_checker"
               
        MsgBox Prompt:="App execution caused unit set to fall out of scope. Some/all units got reset to default:" _
                       & vbNewLine & "Model:" & THERMO_MODEL _
                       & vbNewLine & "pref = " & REF_P _
                       & vbNewLine & "Tref = " & REF_T _
                       & vbNewLine & "Unit T: " & UNIT_T _
                       & vbNewLine & "Unit p: " & UNIT_P _
                       & vbNewLine & "Unit E: " & UNIT_E _
                       & vbNewLine & "Unit m: " & UNIT_M _
                       & vbNewLine & "Unit V: " & UNIT_V _
                       & vbNewLine & "Unit N: " & UNIT_N _
                       & vbNewLine & "Unit base: " & UNIT_BASE, _
               Buttons:=vbOKOnly + vbExclamation, _
               Title:="User defined unit reset"
               
    End If

End Sub


