VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UF_UD_reference_conditions 
   Caption         =   "Set reference conditions"
   ClientHeight    =   1635
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4440
   OleObjectBlob   =   "UF_UD_reference_conditions.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UF_UD_reference_conditions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.UserInterphaseElements")
''-----------------------------------------------------------------------------------------------''

Option Explicit

Private Sub UserForm_Initialize()
    Me.Top = Application.Top + (Application.UsableHeight / 2) - (Me.Height / 2)
    Me.Left = Application.Left + (Application.UsableWidth / 2) - (Me.Width / 2)

End Sub

Private Sub Button_OK_Click()
    end_proceedure
    
End Sub

Private Sub InB_reftemp_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 2 And KeyCode = vbKeyReturn Then
        end_proceedure
        
    ElseIf Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_UD_reference_conditions
    
    End If

End Sub

Private Sub InB_refpress_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 2 And KeyCode = vbKeyReturn Then
        end_proceedure
        
    ElseIf Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_UD_reference_conditions
    
    End If

End Sub

Private Sub Button_OK_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_UD_reference_conditions
    
    End If

End Sub

Private Sub UserForm_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 2 And KeyCode = vbKeyReturn Then
        end_proceedure
        
    ElseIf Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_UD_reference_conditions
    
    End If

End Sub

Private Sub end_proceedure()
    If InB_refpress.value = vbNullString Then
        REF_P = 100000
    
    ElseIf IsNumeric(InB_refpress.value) Then
        REF_P = CDbl(Replace(InB_refpress.value, ",", "."))
        If REF_P < 1000# Or REF_P > 100000000# Then
            MsgBox "For general clarity reference pressure values can be set only in range between 1kPa and 100MPa, please provide appropriate value", vbOKOnly + vbExclamation, "Value out of scope"
            InB_refpress.SetFocus
            Exit Sub
            
        End If
        
    Else
        MsgBox "Pressure input not allowed, please use numeric values.", vbOKOnly + vbExclamation, "Invalid input"
        InB_refpress.SetFocus
        Exit Sub
        
    End If
    
    If InB_refTemp.value = vbNullString Then
        REF_T = 273.15
    
    ElseIf IsNumeric(InB_refTemp.value) Then
        REF_T = CDbl(Replace(InB_refTemp.value, ",", "."))
        If REF_T < 200# Or REF_T > 1500# Then
            MsgBox "For general clarity reference temperature values can be set only in range between 200K and 1500K, please provide appropriate value", vbOKOnly + vbExclamation, "Value out of scope"
            InB_refTemp.SetFocus
            Exit Sub
            
        End If
        
    Else
        MsgBox "Temperature input not allowed, please use numeric values.", vbOKOnly + vbExclamation, "Invalid input"
        InB_refTemp.SetFocus
        Exit Sub
    
    End If
    
    UF_UD_reference_conditions.Hide

End Sub

