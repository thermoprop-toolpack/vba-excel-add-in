Attribute VB_Name = "callingFunctions"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.CallingFunctions")
''-----------------------------------------------------------------------------------------------''

''TODO: Consider wether it is worth to normalize reference state properties so that they are always 0 (also normalize departure functions for them instead of only doing so for sig functions).
''TODO: Add isentropic coefficient functions both for mixture and for component.
''TODO: Add function that checks whether arguments are of the correct type.

Option Explicit

''--Calling functions----------------------------------------------------------------------------''
''--Pure components calling functions section----------------------------------------------------''
Public Function cmp_h(ByVal p As Double, ByVal T As Double, ByVal cmp_symbol As String, _
                      Optional ByVal cmp_phase As String = "gas") As Variant
Attribute cmp_h.VB_Description = "Returns specific molar enthalpy with respect to reference conditions."
Attribute cmp_h.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific enthalpy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T, cmp_phase)
    
    cmp_h = oCmp.h * unit_converter(UNIT_BASE, "h", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function

err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_h"
    cmp_h = "Failed to calculate cmp_h - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_hf(ByVal cmp_symbol As String, _
                       Optional ByVal cmp_phase As String = "gas") As Variant
Attribute cmp_hf.VB_Description = "Returns standard enthalpy of formation of the component in p = 100000Pa, T = 298.15K."
Attribute cmp_hf.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific heat of formation in STD conditions of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, phase:=cmp_phase)
    
    cmp_hf = oCmp.hf * unit_converter(UNIT_BASE, "hf", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_hf"
    cmp_hf = "Failed to calculate cmp_hf - check: " & ERR_LOG_PATH & " for details."
    
End Function

Public Function cmp_u(ByVal p As Double, ByVal T As Double, _
                      ByVal cmp_symbol As String) As Variant
Attribute cmp_u.VB_Description = "Returns specific molar internal energy with respect to reference conditions."
Attribute cmp_u.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific internal energy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_u = oCmp.u * unit_converter(UNIT_BASE, "u", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_u"
    cmp_u = "Failed to calculate cmp_u - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_g(ByVal p As Double, ByVal T As Double, _
                      ByVal cmp_symbol As String) As Variant
Attribute cmp_g.VB_Description = "Returns specific molar gibbs free energy."
Attribute cmp_g.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific Gibbs free energy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_g = oCmp.g * unit_converter(UNIT_BASE, "g", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_g"
    cmp_g = "Failed to calculate cmp_g - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_f(ByVal p As Double, ByVal T As Double, _
                      ByVal cmp_symbol As String) As Variant
    ''Specific Helmholtz free energy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_f = oCmp.f * unit_converter(UNIT_BASE, "f", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_f"
    cmp_f = "Failed to calculate cmp_f - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_s(ByVal p As Double, ByVal T As Double, _
                      ByVal cmp_symbol As String, Optional ByVal cmp_phase = "gas") As Variant
Attribute cmp_s.VB_Description = "Returns specific molar entropy with respect to reference conditions."
Attribute cmp_s.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific entropy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T, cmp_phase)
    
    cmp_s = oCmp.s * unit_converter(UNIT_BASE, "s", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_s"
    cmp_s = "Failed to calculate cmp_s - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_s0(ByVal cmp_symbol As String) As Variant
Attribute cmp_s0.VB_Description = "Returns standard entropy of the component in p = 100000Pa, T = 298.15K."
Attribute cmp_s0.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific entropy of pure component in STD conditions calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol)
    
    cmp_s0 = oCmp.s0 * unit_converter(UNIT_BASE, "s0", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_s0"
    cmp_s0 = "Failed to calculate cmp_s0 - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_x(ByVal p As Double, ByVal T As Double, _
                      ByVal cmp_symbol As String) As Variant
Attribute cmp_x.VB_Description = "Returns specific molar physical exergy with respect to reference conditions."
Attribute cmp_x.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific physical exergy of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_x = oCmp.x * unit_converter(UNIT_BASE, "x", oCmp.MM, oCmp.v)

normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_x"
    cmp_x = "Failed to calculate cmp_x - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_cp(ByVal p As Double, ByVal T As Double, _
                       ByVal cmp_symbol As String) As Variant
Attribute cmp_cp.VB_Description = "Returns specific heat at constant presure."
Attribute cmp_cp.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific heat capacity at constant pressure of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_cp = oCmp.cp * unit_converter(UNIT_BASE, "cp", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_cp"
    cmp_cp = "Failed to calculate cmp_cp - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_cv(ByVal p As Double, ByVal T As Double, _
                       ByVal cmp_symbol As String) As Variant
Attribute cmp_cv.VB_Description = "Returns specific heat at constant volume."
Attribute cmp_cv.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific heat capacity at constant volume of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_cv = oCmp.cv * unit_converter(UNIT_BASE, "cv", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_cv"
    cmp_cv = "Failed to calculate cmp_cv - check: " & ERR_LOG_PATH & " for details."
    
End Function

Public Function cmp_fug(ByVal p As Double, ByVal T As Double, ByVal cmp_symbol As String, _
                        Optional ByVal cmp_phase As String = "gas") As Variant
Attribute cmp_fug.VB_Description = "Returns fugacity coefficient for given phase."
Attribute cmp_fug.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Fugacity coefficient of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T, cmp_phase)
    
    cmp_fug = oCmp.fug
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_fug"
    cmp_fug = "Failed to calculate cmp_fug - check: " & ERR_LOG_PATH & " for details."
    
End Function

Public Function cmp_Z(ByVal p As Double, ByVal T As Double, ByVal cmp_symbol As String, _
                      Optional ByVal cmp_phase As String = "gas") As Variant
Attribute cmp_Z.VB_Description = "Returns compressibility."
Attribute cmp_Z.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Compressibility of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T, cmp_phase)
    
    cmp_Z = oCmp.Z
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_Z"
    cmp_Z = "Failed to calculate cmp_Z - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_v(ByVal p As Double, ByVal T As Double, ByVal cmp_symbol As String) As Variant
Attribute cmp_v.VB_Description = "Returns specific molar volume."
Attribute cmp_v.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific volume of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_v = oCmp.v * unit_converter(UNIT_BASE, "v", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_v"
    cmp_v = "Failed to calculate cmp_v - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_rho(ByVal p As Double, ByVal T As Double, _
                        ByVal cmp_symbol As String) As Variant
Attribute cmp_rho.VB_Description = "Returns density."
Attribute cmp_rho.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Density of pure component calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = create_cmp(cmp_symbol, p, T)
    
    cmp_rho = oCmp.rho * unit_converter(UNIT_BASE, "rho", oCmp.MM, oCmp.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_rho"
    cmp_rho = "Failed to calculate cmp_rho - check: " & ERR_LOG_PATH & " for details."

End Function

''--Pure component critical properties, acentricity and molar mass calling functions section-----''
Public Function cmp_Tc(ByVal cmp_symbol As String) As Variant
Attribute cmp_Tc.VB_Description = "Returns critical temperature of the component."
Attribute cmp_Tc.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component critical temperature property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_Tc = convert_temperature(buff(1, 1), "K", UNIT_T)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_Tc"
    cmp_Tc = "Failed to calculate cmp_Tc - check: " & ERR_LOG_PATH & " for details."

End Function

''--Pure component real gas properties callers---------------------------------------------------''
Public Function cmp_pc(ByVal cmp_symbol As String) As Variant
Attribute cmp_pc.VB_Description = "Returns critical pressure of the component."
Attribute cmp_pc.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component critical pressure property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_pc = convert_pressure(buff(1, 2), "Pa", UNIT_P)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_pc"
    cmp_pc = "Failed to calculate cmp_pc - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_vc(ByVal cmp_symbol As String) As Variant
Attribute cmp_vc.VB_Description = "Returns critical specific volume of the component."
Attribute cmp_vc.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component critical specific volume property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_vc = buff(1, 3) * unit_converter(UNIT_BASE, "vc", cmp_MM(cmp_symbol))
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_vc"
    cmp_vc = "Failed to calculate cmp_vc - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_Zc(ByVal cmp_symbol As String) As Variant
Attribute cmp_Zc.VB_Description = "Returns critical compressibility of the component."
Attribute cmp_Zc.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component critical compressibility property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_Zc = buff(1, 4)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_Zc"
    cmp_Zc = "Failed to calculate cmp_Zc - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_acc(ByVal cmp_symbol As String) As Variant
Attribute cmp_acc.VB_Description = "Returns accentric factor of the component."
Attribute cmp_acc.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component acentricity factor property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_acc = buff(1, 5)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_acc"
    cmp_acc = "Failed to calculate cmp_acc - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function cmp_MM(ByVal cmp_symbol As String) As Variant
Attribute cmp_MM.VB_Description = "Returns molar mass of the component."
Attribute cmp_MM.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Component molar mass property calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    Dim buff As Variant
    buff = get_cmp_rg_coeffs(cmp_symbol)
    cmp_MM = buff(1, 6) * unit_converter("mole", "MM")
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.cmp_MM"
    cmp_MM = "Failed to calculate cmp_MM - check: " & ERR_LOG_PATH & " for details."

End Function

''--Pure gas properties callers------------------------------------------------------------------''
Public Function find_cmp_sig_coeffs(ByVal cmp_name As String, ByVal cmp_T As Double, _
                                    Optional ByVal cmp_phase As String = "gas") As Variant()
Attribute find_cmp_sig_coeffs.VB_Description = "Fetches semi ideal gas coefficients of the component. Returns 10 element array."
Attribute find_cmp_sig_coeffs.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    
    global_unit_checker
    cmp_T = convert_temperature(cmp_T, UNIT_T, "K", False)
    find_cmp_sig_coeffs = get_cmp_sig_coeffs(cmp_name, cmp_T, cmp_phase)
    
normal_exit:
    Exit Function

err_gate:
    Select Case Err.number
        Case vbObjectError + ERR_WRONG_CMP
            find_cmp_sig_coeffs = Array("Failed to find component: " & cmp_name & _
                                        " - check: " & ERR_LOG_PATH & " for details.")
        
        Case vbObjectError + ERR_WRONG_PHS
            find_cmp_sig_coeffs = Array("Failed to find component: " & cmp_name & _
                                        " in given phase - check: " & ERR_LOG_PATH & " for details.")
        
        Case vbObjectError + ERR_WRONG_TMP
            find_cmp_sig_coeffs = Array("Failed to find component: " & cmp_name & _
                                        " in given temperature - check: " & ERR_LOG_PATH & " for details.")
        
        Case Default
            find_cmp_sig_coeffs = Array("Runtime error encountered - check: " _
                                        & ERR_LOG_PATH & " for details.")
    
    End Select
    
    logger log_file_path:=ERR_LOG_PATH, _
           info_type:="Error code: " + Str(err_num_checker(Err.number)), _
           message:=Err.Description, _
           procedure:=Err.Source & "<-" & "callingFunctions.find_cmp_sig_coeffs"

End Function

Public Function find_cmp_rg_coeffs(ByVal cmp_name As String) As Variant()
Attribute find_cmp_rg_coeffs.VB_Description = "Fetches gas properties used in PR-EOS calculation. Returns 6 element array."
Attribute find_cmp_rg_coeffs.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    
    global_unit_checker
    find_cmp_rg_coeffs = get_cmp_rg_coeffs(cmp_name)
    
normal_exit:
    Exit Function

err_gate:
    Select Case Err.number
        Case vbObjectError + ERR_WRONG_CMP
            find_cmp_rg_coeffs = Array("Failed to find component: " & cmp_name & _
                                       " - check: " & ERR_LOG_PATH & " for details.")
        
        Case Default
            find_cmp_rg_coeffs = Array("Runtime error encountered - check: " _
                                        & ERR_LOG_PATH & " for details.")
            
    End Select
    
    logger log_file_path:=ERR_LOG_PATH, _
           info_type:="Error code: " + Str(err_num_checker(Err.number)), _
           message:=Err.Description, _
           procedure:=Err.Source & "<-" & "callingFunctions.find_cmp_rg_coeffs"


End Function

''--Mixture calling functions section------------------------------------------------------------''
Public Function mxt_MM(ByRef components As Range, ByRef fractions As Range) As Variant
Attribute mxt_MM.VB_Description = "Returns molar mass of the component."
Attribute mxt_MM.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Molar mass of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions))
    
    mxt_MM = oMxt.MM * unit_converter("mole", "MM")
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_MM"
    mxt_MM = "Failed to calculate mxt_MM - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_v(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_v.VB_Description = "Returns specific molar volume."
Attribute mxt_v.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific volume of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_v = oMxt.v * unit_converter(UNIT_BASE, "v", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_v"
    mxt_v = "Failed to calculate mxt_v - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_rho(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                        ByRef fractions As Range) As Variant
Attribute mxt_rho.VB_Description = "Returns density. "
Attribute mxt_rho.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Density of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_rho = oMxt.rho * unit_converter(UNIT_BASE, "rho", oMxt.MM, oMxt.v)

normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_rho"
    mxt_rho = "Failed to calculate mxt_rho - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_Z(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_Z.VB_Description = "Returns compressibility."
Attribute mxt_Z.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Compressibility of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_Z = oMxt.Z

normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_Z"
    mxt_Z = "Failed to calculate mxt_Z - check: " & ERR_LOG_PATH & " for details."
    
End Function

Public Function mxt_h(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_h.VB_Description = "Returns specific molar enthalpy with respect to reference conditions."
Attribute mxt_h.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific enthalpy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_h = oMxt.h * unit_converter(UNIT_BASE, "h", oMxt.MM, oMxt.v)

normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_h"
    mxt_h = "Failed to calculate mxt_h - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_x(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_x.VB_Description = "Returns specific molar physical exergy with respect to to reference conditions."
Attribute mxt_x.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific exergy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_x = oMxt.x * unit_converter(UNIT_BASE, "x", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_x"
    mxt_x = "Failed to calculate mxt_x - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_g(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_g.VB_Description = "Returns specific molar gibbs free energy."
Attribute mxt_g.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific Gibbs free energy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_g = oMxt.g * unit_converter(UNIT_BASE, "g", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_g"
    mxt_g = "Failed to calculate mxt_g - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_f(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
    ''Specific Helmholtz free energy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_f = oMxt.f * unit_converter(UNIT_BASE, "f", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_f"
    mxt_f = "Failed to calculate mxt_f - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_u(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_u.VB_Description = "Returns specific molar internal energy with respect to reference conditions."
Attribute mxt_u.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific internal energy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_u = oMxt.u * unit_converter(UNIT_BASE, "u", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_u"
    mxt_u = "Failed to calculate mxt_u - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_s(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                      ByRef fractions As Range) As Variant
Attribute mxt_s.VB_Description = "Returns specific molar entropy with respect to reference conditions."
Attribute mxt_s.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Specific entropy of mixture calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_s = oMxt.s * unit_converter(UNIT_BASE, "s", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_s"
    mxt_s = "Failed to calculate mxt_s - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_cp(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                       ByRef fractions As Range) As Variant
Attribute mxt_cp.VB_Description = "Returns specific heat at constant presure."
Attribute mxt_cp.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Heat capacity of mixture at constant pressure calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker
    
    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_cp = oMxt.cp * unit_converter(UNIT_BASE, "cp", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_cp"
    mxt_cp = "Failed to calculate mxt_cp - check: " & ERR_LOG_PATH & " for details."

End Function

Public Function mxt_cv(ByVal p As Double, ByVal T As Double, ByRef components As Range, _
                       ByRef fractions As Range) As Variant
Attribute mxt_cv.VB_Description = "Returns specific heat at constant volume."
Attribute mxt_cv.VB_ProcData.VB_Invoke_Func = " \n14"
    ''Heat capacity of mixture at constant volume calling function
    On Error GoTo err_gate
    
    ''Checks if all global units are set properly
    global_unit_checker

    vec_len_check components:=components, fractions:=fractions

    Dim oMxt As IMxtCallingFunctions
    Set oMxt = create_mxt(cmps_list(components), cmps_frcs(fractions), p, T)
    
    mxt_cv = oMxt.cv * unit_converter(UNIT_BASE, "cv", oMxt.MM, oMxt.v)
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "callingFunctions.mxt_cv"
    mxt_cv = "Failed to calculate mxt_cv - check: " & ERR_LOG_PATH & " for details."

End Function

''--Composition converters-------------------------------------------------------------------------------------------------------------------------------
''--Functions that are to be used when composition vector is available in one form and is needed in another----------------------------------------------
''--i.e. mole is available and mass is needed or mass is available and mole is needed--------------------------------------------------------------------

Public Function convert_mole_to_mass_frcs(ByRef components As Range, ByRef fractions As Range) As Double()
    ''NOTE: Consider whether to add transposition here. So that funciton detects whether row or column is argument in the input and acts accordingly
    On Error GoTo err_gate
    
    vec_len_check components:=components, fractions:=fractions
    
    Dim cmp_names() As String
    Dim cmp_fracs() As Double
    Dim cmp_MMs() As Double
    
    Dim cmps_qt As Integer
    cmps_qt = components.Cells.Count
    
    ReDim cmp_names(1 To cmps_qt)
    ReDim cmp_fracs(1 To cmps_qt)
    ReDim cmp_MMs(1 To cmps_qt)
    
    cmp_names = cmps_list(components)
    cmp_fracs = cmps_frcs(fractions)
    
    Dim MM_avg As Double
    MM_avg = 0#
    
    Dim i As Integer
    For i = LBound(cmp_fracs) To UBound(cmp_fracs)
        cmp_MMs(i) = cmp_MM(cmp_names(i))
        MM_avg = MM_avg + cmp_fracs(i) * cmp_MMs(i)
    
    Next i
    
    Dim buff() As Double
    ReDim buff(1 To cmps_qt)
    
    For i = LBound(cmp_fracs) To UBound(cmp_fracs)
        buff(i) = cmp_fracs(i) * cmp_MMs(i) / MM_avg
    
    Next i
    
    convert_mole_to_mass_frcs = buff
    
normal_exit:
    Exit Function

err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_mole_to_mass_frcs"
    convert_mole_to_mass_frcs = Array("Failed to convert composition - check: " & ERR_LOG_PATH & " for details.")

End Function

Public Function convert_mass_to_mole_frcs(ByRef components As Range, ByRef fractions As Range) As Double()
    On Error GoTo err_gate
    
    vec_len_check components:=components, fractions:=fractions
    
    Dim cmp_names() As String
    Dim cmp_fracs() As Double
    Dim cmp_MMs() As Double
    
    Dim cmps_qt As Integer
    cmps_qt = components.Cells.Count
    
    ReDim cmp_names(1 To cmps_qt)
    ReDim cmp_fracs(1 To cmps_qt)
    ReDim cmp_MMs(1 To cmps_qt)
    
    cmp_names = cmps_list(components)
    cmp_fracs = cmps_frcs(fractions)
    
    Dim MM_avg As Double
    MM_avg = 0#
    
    Dim i As Integer
    For i = LBound(cmp_fracs) To UBound(cmp_fracs)
        cmp_MMs(i) = cmp_MM(cmp_names(i))
        MM_avg = MM_avg + cmp_fracs(i) / cmp_MMs(i)
    
    Next i
    
    MM_avg = MM_avg ^ (-1)
    
    Dim buff() As Double
    ReDim buff(1 To cmps_qt)
    
    For i = LBound(cmp_fracs) To UBound(cmp_fracs)
        buff(i) = cmp_fracs(i) * MM_avg / cmp_MMs(i)
    
    Next i
    
    convert_mass_to_mole_frcs = buff
    
normal_exit:
    Exit Function

err_gate:
    logger log_file_path:=ERR_LOG_PATH, info_type:="Error code: " & Str(err_num_checker(Err.number)), _
           message:=Err.Description, procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_mass_to_mole_frcs"
    convert_mass_to_mole_frcs = Array("Failed to convert composition - check: " & ERR_LOG_PATH & " for details.")

End Function

''--Utility functions------------------------------------------------------------------------------------------------------------------------------------
''--Functions connected to input check quality: checker of length equality for component names and compositions vector,----------------------------------
''--Names vector generator and composition vector generator with consistency checker---------------------------------------------------------------------
Private Function vec_len_check(ByRef components As Range, ByRef fractions As Range) As Boolean
    If components.Cells.Count = fractions.Cells.Count Then
        vec_len_check = True
        
    Else
        Err.Raise vbObjectError + ERR_WRCMV_LEN, "callingFunctions.vec_len_check", _
                  "Selected vectors are of missmatched lenghts, components have " & _
                  Str(components.Cells.Count) & " elements, while fractions have " & _
                  Str(fractions.Cells.Count) & " elements."
                  
    End If

End Function

Private Function cmps_list(ByRef components As Range) As String()
    Dim i As Integer
    Dim returnVal() As String
    Dim cl As Range
    
    On Error GoTo err_gate
    
    ReDim returnVal(1 To components.Cells.Count)
    i = LBound(returnVal)
    
    Dim buff_value As Variant
    For Each cl In components.Cells
    buff_value = cl.value
    ''NOTE: This is wrong type of check there's no dedicated function to check if something is a string, one has to be written.
        If IsNumeric(buff_value) = True Then
            Err.Raise vbObjectError + ERR_WRCMP_ISN, "callingFunctions.cmps_list", _
                      "Component in cell: " & cl.Address & " is not of string type."
        
        Else
            returnVal(i) = buff_value
            i = i + 1
        
        End If
    
    Next cl
    
    cmps_list = returnVal

normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "callingFunctions.cmps_list", Err.Description

End Function

Private Function cmps_frcs(ByRef fractions As Range) As Double()
    Dim i As Integer
    Dim returnVal() As Double, totQty As Double
    Dim cl As Range
    
    On Error GoTo err_gate
    
    ReDim returnVal(1 To fractions.Cells.Count)
    i = LBound(returnVal)
    totQty = 0#
    
    Dim buff_value As Variant
    For Each cl In fractions
        buff_value = cl.value
        If Not IsNumeric(buff_value) Then
            Err.Raise vbObjectError + ERR_WRFRC_NTN, "callingFunctions.cmps_frcs", _
                      "Fraction in cell: " & cl.Address & " is not of numeric type."
        
        End If
    
        If buff_value < 0 Then
            logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_CMPFR_LTZ, _
                   message:="Component fraction < 0. Setting to 0...", _
                   procedure:="callingFunctions.cmps_frcs"
            returnVal(i) = 0
            
        Else
            returnVal(i) = buff_value
            
        End If
        
        totQty = totQty + returnVal(i)
        i = i + 1
    
    Next cl
    
    If Abs(totQty - 1) > 0.0001 Then
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_CMPFR_SUM, _
               message:="Sum of component fractions is other than 1 (actual: " _
                        & Str(totQty) & "). Normalizing components fractions...", _
               procedure:="callingFunctions.cmps_frcs"
        For i = LBound(returnVal) To UBound(returnVal)
            returnVal(i) = returnVal(i) / totQty
            
        Next i
        
    End If
    
    cmps_frcs = returnVal
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "callingFunctions.cmps_frcs", Err.Description

End Function

Private Function unit_converter(ByVal base As String, ByVal type_of_conversion As String, _
                                Optional ByVal molar_mass As Double = 0#, _
                                Optional ByVal spec_volume As Double = 0#) As Double
    ''Function produced desired unit conversion coefficient based on base of calculation
    On Error GoTo err_gate
    Dim base_conv_fct As Double
    
    Select Case base
        Case "mole"
            base_conv_fct = 1 / convert_particles(1, "mol", UNIT_N)
        
        Case "mass"
            If molar_mass = 0 Then
                Err.Raise vbObjectError + ERR_MBMMR_DBZ, "callingFunctions.unit_converter", _
                          "Molar mass parameter not passed to converter function for mass" & _
                          " based calculation."
            
            End If
            base_conv_fct = 1 / (convert_particles(1, "mol", "kmol") * molar_mass _
                            * convert_mass(1, "kg", UNIT_M))
        
        Case "volume"
            If spec_volume = 0 Then
                Err.Raise vbObjectError + ERR_VBSVR_DBZ, "callingFunctions.unit_converter", _
                          "Specific volume parameter not passed to converter function for" & _
                          " volume based calculation."
            
            End If
            base_conv_fct = 1 / (spec_volume * convert_volume(1, "m3", UNIT_V))
        
        Case Default
            Err.Raise vbObjectError + ERR_BASEC_VNS, "callingFunctions.unit_converter", _
                      "Unrecognized base variable of: " & base & "check documentation" & _
                      " for allowed args."
        
    End Select
    
    Select Case type_of_conversion
        Case "h", "hf", "u", "g", "x", "f"
            unit_converter = convert_energy(1, "J", UNIT_E) * base_conv_fct
            
        Case "s", "s0", "cp", "cv"
            unit_converter = convert_energy(1, "J", UNIT_E) * base_conv_fct _
                            / (convert_temperature(2, "K", UNIT_T) _
                            - convert_temperature(1, "K", UNIT_T))
        
        Case "v", "vc"
            Select Case base
                Case "mole"
                    unit_converter = convert_volume(1, "m3", UNIT_V) _
                                     / convert_particles(1, "mol", UNIT_N)
                                     
                Case "mass", "volume"
                    unit_converter = convert_volume(1, "m3", UNIT_V) _
                                    / (convert_particles(1, "mol", "kmol") * molar_mass)
                
            End Select
            
        Case "rho"
            Select Case base
                Case "mole"
                    unit_converter = convert_particles(1, "mol", UNIT_N) _
                                     / convert_volume(1, "m3", UNIT_V)
                
                Case "mass", "volume"
                    unit_converter = convert_particles(1, "mol", "kmol") * molar_mass _
                                     / convert_volume(1, "m3", UNIT_V)
        
            End Select
            
        Case "MM"
            unit_converter = convert_mass(1, "kg", UNIT_M) / convert_particles(1, "kmol", UNIT_N)
        
        Case Default
            Err.Raise vbObjectError + ERR_UNRCN_CTP, "callingFunctions.unit_converter", _
                      "Unrecognized type_of_conversion variable of: " & type_of_conversion & _
                      "check documentation for allowed args."
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "callingFunctions.unit_converter", Err.Description

End Function

Private Function err_num_checker(ByVal err_num As Long) As Long
    ''Function checks whether error was raised by application or by user based on err.Number value
    If err_num < 0 Then
        err_num_checker = err_num - vbObjectError
        
    Else
        err_num_checker = err_num
        
    End If

End Function

''--Wrapper functions----------------------------------------------------------------------------''
''--Functions used to initialize component and mixture objects-----------------------------------''
Private Function create_cmp(ByVal name As String, Optional ByVal p As Double = 0#, _
                           Optional ByVal T As Double = 0#, _
                           Optional ByVal phase As String = "gas") As ICmpCallingFunctions
    Dim oCmp As ICmpCallingFunctions
    Set oCmp = New clsComponent
    
    On Error GoTo err_gate
    
    ''SI converter
    p = convert_pressure(p, UNIT_P, "Pa")
    T = convert_temperature(T, UNIT_T, "K")
    
    ''Class initialization function call
    oCmp.Init symbol:=name, p:=p, T:=T, phase:=phase
    
    Set create_cmp = oCmp
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, Err.Source & "<-" & "callingFunctions.createCmp", Err.Description

End Function

Private Function create_mxt(ByRef componentNames() As String, ByRef molarFraction() As Double, _
                           Optional ByVal p As Double = 0#, _
                           Optional ByVal T As Double = 0#) As IMxtCallingFunctions
    Dim oMxt As IMxtCallingFunctions
    Set oMxt = New clsMixture
    
    On Error GoTo err_gate
    
    ''SI converter
    p = convert_pressure(p, UNIT_P, "Pa")
    T = convert_temperature(T, UNIT_T, "K")
    
    ''Class initialization function call
    oMxt.Init components:=componentNames, composition:=molarFraction, p:=p, T:=T
    
    Set create_mxt = oMxt
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, Err.Source & "<-" & "callingFunctions.createMxt", Err.Description

End Function
