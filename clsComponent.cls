VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsComponent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.ClassModules")
''-----------------------------------------------------------------------------------------------''

Option Explicit

''--Variables------------------------------------------------------------------------------------------------------------------------
''Chemical symbol of the component e.g. CO2, O2, CH4, etc.
Private cmp_symbol As String, cmp_phase As String

''Critical point parameters, eccentricity and molar mass of the component, as well as a, b, k from PR EOS
Private cmp_Tc As Double, cmp_pc As Double, cmp_accf As Double, cmp_MM As Double
Private cmp_a As Double, cmp_b As Double, cmp_k As Double

''SemiIdeal gas parameters 9 coefficients from NASA Glenn properties file
Private SIG_coeffs As Variant

''Point pressure, temperature (default set to reference conditions)
Private cmp_T As Double, cmp_p As Double, cmp_Tref As Double, cmp_pref As Double

Implements ICmpCallingFunctions
Implements ICmpAPI

''--Properties------------------------------------------------------------------------------------------------------------------------
''--ICmpCallingFunctions implementation properties-----------------------------------------------''
Private Property Get ICmpCallingFunctions_symbol() As String
    ICmpCallingFunctions_symbol = cmp_symbol

End Property

Private Property Get ICmpCallingFunctions_Tc() As Double
    ICmpCallingFunctions_Tc = cmp_Tc
    
End Property

Private Property Get ICmpCallingFunctions_pc() As Double
    ICmpCallingFunctions_pc = cmp_pc

End Property

Private Property Get ICmpCallingFunctions_accf() As Double
    ICmpCallingFunctions_accf = cmp_accf

End Property

Private Property Get ICmpCallingFunctions_MM() As Double
    ICmpCallingFunctions_MM = cmp_MM

End Property

Private Property Get ICmpCallingFunctions_T() As Double
    ICmpCallingFunctions_T = cmp_T
    
End Property

Private Property Get ICmpCallingFunctions_p() As Double
    ICmpCallingFunctions_p = cmp_p

End Property

Private Property Get ICmpCallingFunctions_v() As Double
    ICmpCallingFunctions_v = cmp_specvolume(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_rho() As Double
    ICmpCallingFunctions_rho = cmp_density(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_Tref() As Double
    ICmpCallingFunctions_Tref = cmp_Tref

End Property

Private Property Get ICmpCallingFunctions_pref() As Double
    ICmpCallingFunctions_pref = cmp_pref

End Property

Private Property Get ICmpCallingFunctions_h() As Double
    ICmpCallingFunctions_h = cmp_enth(cmp_p, cmp_T)
    
End Property

Private Property Get ICmpCallingFunctions_hf() As Double
    ''TODO: Double check whether cmp_Tref is correct temperature here
    ICmpCallingFunctions_hf = cmp_enth_sig(cmp_Tref)

End Property

Private Property Get ICmpCallingFunctions_u() As Double
    ICmpCallingFunctions_u = cmp_intener(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_g() As Double
    ICmpCallingFunctions_g = cmp_gibbs(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_f() As Double
    ICmpCallingFunctions_f = cmp_helmholtz(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_s() As Double
    ICmpCallingFunctions_s = cmp_entr(cmp_p, cmp_T)
    
End Property

Private Property Get ICmpCallingFunctions_s0() As Double
    ''TODO: Double check whether cmp_Tref is correct temperature here
    ICmpCallingFunctions_s0 = cmp_entr_sig(cmp_Tref)

End Property

Private Property Get ICmpCallingFunctions_cp() As Double
    ICmpCallingFunctions_cp = cmp_heatcp(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_cv() As Double
    ICmpCallingFunctions_cv = cmp_heatcv(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_x() As Double
    ICmpCallingFunctions_x = cmp_exer(cmp_p, cmp_T)

End Property

Private Property Get ICmpCallingFunctions_Z() As Double
    ICmpCallingFunctions_Z = compr(cmp_p, cmp_T, cmp_phase)
    
End Property

Private Property Get ICmpCallingFunctions_fug() As Double
    ICmpCallingFunctions_fug = cmp_fugacity(cmp_p, cmp_T, cmp_phase)

End Property

Private Sub ICmpCallingFunctions_Init(ByVal symbol As String, Optional ByVal p As Double = 0#, _
                Optional ByVal T As Double = 0#, Optional ByVal phase As String = "gas")
    Init symbol:=symbol, p:=p, T:=T, phase:=phase

End Sub

''--ICmpAPI implementation properties------------------------------------------------------------''
Private Property Get ICmpAPI_symbol() As String
    ICmpAPI_symbol = cmp_symbol

End Property

Private Property Let ICmpAPI_symbol(ByVal symbol As String)
    cmp_symbol = symbol

End Property

Private Property Get ICmpAPI_Tc() As Double
    ICmpAPI_Tc = cmp_Tc

End Property

Private Property Get ICmpAPI_pc() As Double
    ICmpAPI_pc = cmp_pc

End Property

Private Property Get ICmpAPI_accf() As Double
    ICmpAPI_accf = cmp_accf

End Property

Private Property Get ICmpAPI_MM() As Double
    ICmpAPI_MM = cmp_MM

End Property

Private Property Get ICmpAPI_T() As Double
    ICmpAPI_T = cmp_T

End Property

Private Property Let ICmpAPI_T(ByVal T As Double)
    cmp_T = T

End Property

Private Property Get ICmpAPI_p() As Double
    ICmpAPI_p = cmp_p

End Property

Private Property Let ICmpAPI_p(ByVal p As Double)
    cmp_p = p

End Property

Private Property Get ICmpAPI_v() As Double
    ICmpAPI_v = cmp_specvolume(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_rho() As Double
    ICmpAPI_rho = cmp_density(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_Tref() As Double
    ICmpAPI_Tref = cmp_Tref

End Property

Private Property Let ICmpAPI_Tref(ByVal Tref As Double)
    cmp_Tref = Tref

End Property

Private Property Get ICmpAPI_pref() As Double
    ICmpAPI_pref = cmp_pref

End Property

Private Property Let ICmpAPI_pref(ByVal pref As Double)
    cmp_pref = pref

End Property

Private Property Get ICmpAPI_h() As Double
    ICmpAPI_h = cmp_enth(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_hf() As Double
    ''TODO: Double check whether cmp_Tref is correct temperature here
    ICmpAPI_hf = cmp_enth_sig(cmp_Tref, cmp_phase)

End Property

Private Property Get ICmpAPI_hsig(ByVal T As Double) As Double
    ICmpAPI_hsig = cmp_enth_sig(T)

End Property

Private Property Get ICmpAPI_u() As Double
    ICmpAPI_u = cmp_intener(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_g() As Double
    ICmpAPI_g = cmp_gibbs(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_f() As Double
    ICmpAPI_f = cmp_helmholtz(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_s() As Double
    ICmpAPI_s = cmp_entr(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_s0() As Double
    ''TODO: Double check whether cmp_Tref is correct temperature here
    ICmpAPI_s0 = cmp_entr_sig(cmp_Tref)

End Property

Private Property Get ICmpAPI_ssig(ByVal T As Double) As Double
    ICmpAPI_ssig = cmp_entr_sig(T)

End Property

Private Property Get ICmpAPI_cp() As Double
    ICmpAPI_cp = cmp_heatcp(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_cpsig(ByVal T As Double) As Double
    ICmpAPI_cpsig = cmp_heatcp_sig(cmp_T)

End Property

Private Property Get ICmpAPI_cv() As Double
    ICmpAPI_cv = cmp_heatcv(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_cvsig(ByVal T As Double) As Double
    ICmpAPI_cvsig = cmp_heatcv_sig(cmp_T)

End Property

Private Property Get ICmpAPI_x() As Double
    ICmpAPI_x = cmp_exer(cmp_p, cmp_T)

End Property

Private Property Get ICmpAPI_Z() As Double
    ICmpAPI_Z = compr(cmp_p, cmp_T, cmp_phase)

End Property

Private Property Get ICmpAPI_fug() As Double
    ICmpAPI_fug = cmp_fugacity(cmp_p, cmp_T, cmp_phase)

End Property

Private Property Get ICmpAPI_deltaFugacityPhaseChange() As Double
    ICmpAPI_deltaFugacityPhaseChange = cmp_fugacity(cmp_p, cmp_T, "liquid") _
                                     - cmp_fugacity(cmp_p, cmp_T, "gas")

End Property

Private Property Get ICmpAPI_ac() As Double
    ICmpAPI_ac = cmp_a

End Property

Private Property Get ICmpAPI_bc() As Double
    ICmpAPI_bc = cmp_b

End Property

Private Property Get ICmpAPI_k() As Double
    ICmpAPI_k = cmp_k

End Property

Private Property Get ICmpAPI_alpha() As Double
    ICmpAPI_alpha = alpha_fct(cmp_T)
    
End Property

Private Property Get ICmpAPI_dAlpha() As Double
    ICmpAPI_dAlpha = d_alpha_fct(cmp_T)

End Property

Private Property Get ICmpAPI_d2Alpha() As Double
    ICmpAPI_d2Alpha = d2_alpha_fct(cmp_T)

End Property

Private Sub ICmpAPI_Init(ByVal symbol As String, Optional ByVal p As Double = 0#, _
                         Optional ByVal T As Double = 0#, Optional ByVal phase As String = "gas")
    Init symbol:=symbol, p:=p, T:=T, phase:=phase

End Sub

''--Methods------------------------------------------------------------------------------------------------------------------------
''--Object initialization proceedure called by a wrapper function------------------------------------------------------------------
Private Sub Init(ByVal symbol As String, Optional ByVal p As Double = 0#, _
                Optional ByVal T As Double = 0#, Optional ByVal phase As String = "gas")
    On Error GoTo err_gate
    
    Dim buff() As Variant

    ''These 3 properties are to be linked to setup form on a later date
    cmp_Tref = REF_T
    cmp_pref = REF_P
    cmp_phase = phase
    cmp_symbol = symbol
    
    ''Component pressure and temperature set, in case of too low value warning is returned_
    ''which is non important for hf and s0 properties
    If p > 0 Then
        cmp_p = p
        
    Else
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_PRESS_LEZ, _
               message:="Set pressure value (" & p & _
                        "Pa) is too low, setting cmp_p to reference value of: " & cmp_pref & "Pa.", _
               procedure:="clsComponent.Init"
        cmp_p = cmp_pref
        
    End If
        
    If T > 0 Then
        cmp_T = T
        
    Else
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_TEMPR_LEZ, _
               message:="Set temperature value (" & T & _
                        "K) is too low, setting cmp_T to reference value of: " & cmp_Tref & "K.", _
               procedure:="clsComponent.Init"
        cmp_T = cmp_Tref
               
    End If
    
    ''Initialize real gas properties
    buff = get_cmp_rg_coeffs(cmp_symbol)
    
    cmp_pc = buff(1, 2)
    cmp_Tc = buff(1, 1)
    cmp_accf = buff(1, 5)
    cmp_MM = buff(1, 6)
    
    ''Critical coefficients of PR-EOS model
    cmp_a = 0.45723553 * (R * cmp_Tc) ^ 2 / cmp_pc
    cmp_b = 0.077796074 * R * cmp_Tc / cmp_pc
    cmp_k = 0.37464 + 1.54226 * cmp_accf - 0.26992 * cmp_accf ^ 2
    
normal_exit:
    Exit Sub
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.Init"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.Init"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Sub

''--Thermodynamic properties functions including:-----------------------------------------------------------------------------------------------
''--h, h_sig, s, s_sig, u, x, g, cp, cv, v, rho-------------------------------------------------------------------------------------------------

Private Function cmp_enth(ByVal p As Double, ByVal T As Double) As Double
    ''Enthalpy of the real gas with respect to reference conditions based on PR-EOS model
    ''h_RG = h_IG(T) - h_IG(Tref) + h_dept(p, T)
    ''only sensible enthalpy, standard enthalpy of formation is not included, it can be obrained with separate function
    On Error GoTo err_gate
    
    cmp_enth = cmp_enth_sig(T) - cmp_enth_sig(cmp_Tref) + cmp_enth_dept(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_enth"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_enth"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function cmp_enth_sig(ByVal T As Double, Optional ByVal phase As String = "gas")
    ''Enthalpy function for semi ideal gas
    ''h_IG = dotproduct(a_vect, a_vect_coeffs, T^i_vect, T_ln_vect)
    On Error GoTo err_gate
    
    Dim cmp_coeffs As Variant, cmp_pow_fct As Variant
    Dim cmp_coeffs_mltp As Variant, cmp_ln_vect As Variant
    Dim buff_value As Double
    Dim i As Integer
    
    SIG_coeffs = get_cmp_sig_coeffs(cmp_symbol, T, phase)
    cmp_pow_fct = Array(-2, -1, 0, 1, 2, 3, 4, 0, -1, 0)
    cmp_coeffs_mltp = Array(-1, 1, 1, 0.5, 1 / 3, 0.25, 0.2, 0, 1, 0)
    cmp_ln_vect = Array(1, ln(T), 1, 1, 1, 1, 1, 0, 1, 1)
    
    buff_value = 0#
    
    For i = 1 To 10
        buff_value = buff_value + SIG_coeffs(1, i + 1) * cmp_coeffs_mltp(i - 1) * cmp_ln_vect(i - 1) * T ^ cmp_pow_fct(i - 1)
    
    Next i
    
    cmp_enth_sig = buff_value * T * R
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_enth_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_enth_sig"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_enth_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Enthalpy departure function based on PR-EOS model
    ''h_dept = R * T * (Z - 1 - A / (B * sqrt(8)) * (1 + k * sqrt(Trel / a(T))) * ln((Z + (sqrt(2) + 1) * B)/(Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim Trel As Double
    Dim A As Double, B As Double, Z As Double
    Dim enth_dept As Double
    
    Trel = cmp_trel(T, cmp_Tc)
    A = A_fct(p, T)
    B = B_fct(p, T)
    Z = compr(p, T, cmp_phase)
    
    cmp_enth_dept = R * T * (Z - 1 - A / (sqrt(8) * B) * (1 + cmp_k * sqrt(Trel / alpha_fct(T))) _
                    * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B)))
                    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_enth_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_enth_dept"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
   
End Function

Private Function cmp_intener(ByVal p As Double, ByVal T As Double) As Double
    ''Internal energy of the real gas with respect to reference conditions based on PR-EOS model enthalpy calculation
    ''u_RG = h_RG - R * (Z(p, T) * T - Z(p_ref, T_ref) * T)
    ''where: Z(p, T) * T * R == p * v
    On Error GoTo err_gate
    
    cmp_intener = cmp_enth(p, T) - R * (compr(p, T) * T - compr(cmp_pref, cmp_Tref) * cmp_Tref)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_intener"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_intener"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_entr(ByVal p As Double, ByVal T As Double) As Double
    ''Entropy of the real gas with respect to reference conditions based on PR-EOS model
    ''s_RG = s_IG(T) - s_IG(Tref) - R * ln(p / pref) + s_dept(p, T)
    On Error GoTo err_gate
    
    cmp_entr = cmp_entr_sig(T) - cmp_entr_sig(cmp_Tref) + cmp_entr_dept(p, T) - R * ln(p / cmp_pref)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_entr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_entr"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_entr_sig(ByVal T As Double) As Double
    ''Entropy function for semi ideal gas
    ''s_IG = dotproduct(a_vect, a_vect_coeffs, T^i_vect, T_ln_vect)
    On Error GoTo err_gate
    
    Dim cmp_pow_fct As Variant
    Dim cmp_coeffs_mltp As Variant, cmp_ln_vect As Variant
    Dim buff_value As Double
    Dim i As Integer
    
    SIG_coeffs = get_cmp_sig_coeffs(cmp_symbol, T)
    cmp_pow_fct = Array(-2, -1, 0, 1, 2, 3, 4, 0, 0, 0)
    cmp_coeffs_mltp = Array(-0.5, -1, 1, 1, 0.5, 1 / 3, 0.25, 0, 0, 1)
    cmp_ln_vect = Array(1, 1, ln(T), 1, 1, 1, 1, 0, 1, 1)
    
    buff_value = 0#
    
    For i = 1 To 10
        buff_value = buff_value _
                     + SIG_coeffs(1, i + 1) * cmp_coeffs_mltp(i - 1) _
                     * cmp_ln_vect(i - 1) * T ^ cmp_pow_fct(i - 1)
    
    Next i
    
    cmp_entr_sig = buff_value * R
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_entr_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_entr_sig"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_entr_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Entropy departure function based on PR-EOS model
    ''s_dept = R * ln(Z - B) - (A * R) / (B * sqrt(8)) * k * sqrt(T_rel / a(T)) * ln((Z + (sqrt(2) + 1) * B)/(Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim A As Double, B As Double, Z As Double, T_rel As Double
    
    T_rel = cmp_trel(T, cmp_Tc)
    A = A_fct(p, T)
    B = B_fct(p, T)
    Z = compr(p, T, cmp_phase)
    
    cmp_entr_dept = R * ln(Z - B) - (A * R) / (sqrt(8) * B) * cmp_k * sqrt(T_rel / alpha_fct(T)) _
                    * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B))
                    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_entr_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_entr_dept"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
        
End Function

Private Function cmp_exer(ByVal p As Double, ByVal T As Double) As Double
    ''Physical exergy of the component
    ''x(p, T) = h(p, T) - T_ref * s(p, T)
    On Error GoTo err_gate

    cmp_exer = cmp_enth(p, T) - cmp_Tref * cmp_entr(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_exer"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_exer"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_gibbs(ByVal p As Double, ByVal T As Double) As Double
    ''Gibbs free energy used for reaction equilibrium calculations
    ''g(p, T) = h(p, T) + dh_f(T_std) - T * (s(p, T) + s_0(T_std))
    On Error GoTo err_gate
    
    Dim enthalpy As Double, entropy As Double
    
    enthalpy = cmp_enth(p, T) + cmp_enth_sig(T_std)
    entropy = cmp_entr(p, T) + cmp_entr_sig(T_std)
    cmp_gibbs = enthalpy - T * entropy

normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_gibbs"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_gibbs"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_helmholtz(ByVal p As Double, ByVal T As Double) As Double
    ''Helmholtz free energy use alongside gibbs energy in closed systems
    ''Unlike gibbs it is does not refer to absolute state but rather is relative to reference conditions
    ''f(p, T) = u(p, T) - T * s(p, T)
    
    On Error GoTo err_gate
    
    Dim int_energy As Double, entropy As Double
    
    int_energy = cmp_intener(p, T)
    entropy = cmp_entr(p, T)
    
    cmp_helmholtz = int_energy - T * entropy
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_helmholtz"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_helmholtz"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_heatcp(ByVal p As Double, ByVal T As Double) As Double
    ''Constant pressure specific heat of the real gas based on PR-EOS model
    ''cp_RG(p, T) = cp_IG(T) + cp_dept(p, T)
    On Error GoTo err_gate
    
    cmp_heatcp = cmp_heatcp_sig(T) + cmp_heatcp_dept(p, T)
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcp"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcp"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_heatcp_sig(ByVal T As Double) As Double
    ''Constant pressure specific heat for semi ideal gas
    ''cp_IG = dotproduct(a_vect, T^i_vect)
    On Error GoTo err_gate
    
    Dim cmp_pow_fct As Variant
    Dim cmp_coeffs_mltp As Variant, cmp_ln_vect As Variant
    Dim buff_value As Double
    Dim i As Integer
    
    SIG_coeffs = get_cmp_sig_coeffs(cmp_symbol, T)
    cmp_pow_fct = Array(-2, -1, 0, 1, 2, 3, 4, 0, 0, 0)
    cmp_coeffs_mltp = Array(1, 1, 1, 1, 1, 1, 1, 0, 0, 0)
    cmp_ln_vect = Array(1, 1, 1, 1, 1, 1, 1, 0, 0, 0)
    
    buff_value = 0#
    
    For i = 1 To 10
        buff_value = buff_value _
                     + SIG_coeffs(1, i + 1) * cmp_coeffs_mltp(i - 1) _
                     * cmp_ln_vect(i - 1) * T ^ cmp_pow_fct(i - 1)
    
    Next i
    
    cmp_heatcp_sig = buff_value * R
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcp_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcp_sig"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_heatcp_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Constant pressure specific heat departure function based on PR-EOS model (to simplify equation cv_dept is calculated as well)
    ''cp_dept = cv_dept - R + R / (Z - B) - da/dT * p / (R * T * (Z ^ 2 + 2 * B * Z - B ^ 2)) * (Z + dZ/dT * T)
    On Error GoTo err_gate
    
    Dim B As Double, da_dT As Double
    Dim Z As Double, d_Z As Double
    
    B = B_fct(p, T)
    da_dT = d_alpha_fct(T) * cmp_a
    Z = compr(p, T)
    d_Z = d_compr(p, T)
    
    cmp_heatcp_dept = cmp_heatcv_dept(p, T) - R _
                      + (R / (Z - B) - da_dT * p / (R * T) / (Z ^ 2 + 2 * B * Z - B ^ 2)) _
                      * (Z + d_Z * T)
                      
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcp_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcp_dept"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_heatcv(ByVal p As Double, ByVal T As Double) As Double
    ''Constant volume specific heat of the real gas based on PR-EOS model
    ''cv_RG(p, T) = cv_IG(T) + cv_dept(p, T)
    On Error GoTo err_gate
    
    cmp_heatcv = cmp_heatcv_sig(T) + cmp_heatcv_dept(p, T)
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcv"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcv"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_heatcv_sig(ByVal T As Double) As Double
    ''Constant volume specific heat for semi ideal gas
    ''cv_IG = cp_IG(T) - R
    On Error GoTo err_gate
    
    cmp_heatcv_sig = cmp_heatcp_sig(T) - R
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcv_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcv_sig"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function cmp_heatcv_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Constant volume specific heat departure function based on PR-EOS model
    ''cv_dept = T * d2a/dT2 / (B * sqrt(8)) * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim B As Double, d2a_dT2 As Double
    Dim Z As Double
    
    B = B_fct(p, T)
    d2a_dT2 = d2_alpha_fct(T) * cmp_a
    Z = compr(p, T)
    
    cmp_heatcv_dept = (T * d2a_dT2 / (cmp_b * sqrt(8))) _
                      * (ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B)))
                      
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_heatcv_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_heatcv_dept"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function cmp_fugacity(ByVal p As Double, ByVal T As Double, _
                              Optional ByVal phase As String = "gas") As Double
    ''Fugacity coefficient function based on PR-EOS model
    ''f = exp(Z - 1 - ln(Z - B) - A / (sqrt(8) * B) * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim Z As Double, A As Double, B As Double
    
    Z = compr(p, T, phase)
    A = A_fct(p, T)
    B = B_fct(p, T)
    
    cmp_fugacity = Exp(Z - 1 - ln(Z - B) - A / (sqrt(8) * B) _
                   * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B)))
                   
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_fugacity"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_fugacity"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function cmp_specvolume(ByVal p As Double, ByVal T As Double) As Double
    ''Molar specific volume of the component based on PR-EOS model
    ''v = z * R * T / p
    On Error GoTo err_gate
    
    cmp_specvolume = compr(p, T) * R * T / p
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_specvolume"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_specvolume"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function cmp_density(ByVal p As Double, ByVal T As Double) As Double
    ''Component density based on PR-EOS model specific volume
    ''rho(p, T) = 1 / v(p, T)
    On Error GoTo err_gate
    
    cmp_density = 1 / cmp_specvolume(p, T)
    
normal_exit:
    Exit Function

err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.cmp_density"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.cmp_density"
        
    End If
    
    Err.Raise Err.number, Err.Source, Err.Description

End Function

''--RG properties calculations and their derivatieves used in PR-EOS model----------------------------------------------------------------------
''--Including Z, dZ/dT, alpha(T), d(alpha)/dT, d2(alpha)/dT2, A, dA/dT, B, dB/dT, T_R, p_R------------------------------------------------------

Private Function compr(ByVal p As Double, ByVal T As Double, _
                       Optional ByVal phase As String = "gas") As Double
    ''Compressibility cubic equation solution from PR-EOS model
    ''Z^3 + (B - 1) * Z ^ 2 + (-3 * B ^ 2 - 2 * B + A) * Z + (B ^ 3 + B ^ 2 - A * B) = 0
    On Error GoTo err_gate
    
    Dim A As Double, B As Double
    Dim a_3 As Double, a_2 As Double, a_1 As Double, a_0 As Double
    Dim p1 As Double, q As Double, Q1 As Double, R1 As Double, D1 As Double
    Dim theta As Double, Z(1 To 3) As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    
    a_3 = 1
    a_2 = B - 1
    a_1 = -3 * B ^ 2 - 2 * B + A
    a_0 = B ^ 3 + B ^ 2 - A * B
    
    p1 = (3 * a_1 - a_2 ^ 2) / 3
    q = (9 * a_1 * a_2 - 27 * a_0 - 2 * a_2 ^ 3) / 27
    
    Q1 = p1 / 3
    R1 = q / 2
    D1 = Q1 ^ 3 + R1 ^ 2
    
    If D1 < 0 Then
        theta = acos(R1 / sqrt(-Q1 ^ 3))
        
        Z(1) = 2 * sqrt(-Q1) * Cos(theta / 3) - a_2 / 3
        Z(2) = 2 * sqrt(-Q1) * Cos((theta + 2 * pi) / 3) - a_2 / 3
        Z(3) = 2 * sqrt(-Q1) * Cos((theta + 4 * pi) / 3) - a_2 / 3
        
        If phase = "gas" Then
            compr = max(Z)
            
        ElseIf phase = "liquid" Then
            compr = min(Z)
            
        End If
        
    Else
        compr = cbrt(R1 + sqrt(D1)) + cbrt(R1 - sqrt(D1)) - a_2 / 3
        
    End If
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.compr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.compr"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_compr(ByVal p As Double, ByVal T As Double, _
                         Optional ByVal phase As String = "gas") As Double
    ''Compressibility equation derivative with respect to temperature at constant pressure from PR-EOS model
    ''            (B - z)(dA/dT)_p + (A - 2 * B - 3 * B ^ 2 + 6 * B * z + 2 * z - z ^ 2)(dB/dT)_p
    ''(dZ/dT)_p = -------------------------------------------------------------------------------
    ''                      3 * z ^ 2 + 2 * (B - 1) * z + (-3 * B ^ 2 - 2 * B + A)
    On Error GoTo err_gate
    
    Dim A As Double, B As Double, dA As Double, dB As Double, Z As Double
    Dim numerator As Double, denominator As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    dA = d_A_fct(p, T)
    dB = d_B_fct(p, T)
    Z = compr(p, T, phase)
    
    numerator = (B - Z) * dA + (A - 2 * B - 3 * B ^ 2 + 6 * B * Z + 2 * Z - Z ^ 2) * dB
    denominator = 3 * Z ^ 2 + 2 * (B - 1) * Z + (-3 * B ^ 2 - 2 * B + A)
    
    d_compr = numerator / denominator
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.d_compr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.d_compr"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function alpha_fct(ByVal T As Double) As Double
    ''PR-EOS alpha coefficient relating particle forces to temperature
    ''alpha(T) = 1 + k * (1 - sqrt(T / T_cr))) ^ 2
    On Error GoTo err_gate
    
    alpha_fct = (1 + cmp_k * (1 - sqrt(cmp_trel(T, cmp_Tc)))) ^ 2
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.alpha_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.alpha_fct"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_alpha_fct(ByVal T As Double) As Double
    ''Partial derivative of alpha_fct with respect to temperature
    ''d(alpha)/dT = -k / sqrt(T * T_cr) * (1 + k * (1 - sqrt(T_rel)))
    On Error GoTo err_gate
    
    d_alpha_fct = -cmp_k / sqrt(T * cmp_Tc) * (1 + cmp_k * (1 - sqrt(cmp_trel(T, cmp_Tc))))
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.d_alpha_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.d_alpha_fct"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d2_alpha_fct(ByVal T As Double) As Double
    ''Second derivative of alpha_fct with respect to temperature
    ''d2(alpha)/dT2 = k * (k + 1) / (sqrt(T_rel) * 2 * T * T_cr)
    On Error GoTo err_gate
    
    d2_alpha_fct = cmp_k * (cmp_k + 1) / (sqrt(cmp_trel(T, cmp_Tc)) * 2 * T * cmp_Tc)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.d2_alpha_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.d2_alpha_fct"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function A_fct(ByVal p As Double, T As Double) As Double
    ''Nondimentional alpha coefficient of PR-EOS
    ''A = a_cr * alpha(T) * p / (R * T) ^ 2
    On Error GoTo err_gate
    
    A_fct = cmp_a * alpha_fct(T) * p / (R * T) ^ 2
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.A_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.A_fct"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_A_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Partial derivative of A coefficient with respect to temperature at constant pressure
    ''(dA/dT)_p = a_c * p / (R * T) ^ 2 * ((d(alpha)/dT)_p - 2 * alpha(T) / T)
    On Error GoTo err_gate
    
    d_A_fct = cmp_a * p / (R * T) ^ 2 * (d_alpha_fct(T) - 2 * alpha_fct(T) / T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsComponent.d_A_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsComponent.d_A_fct"
        
    End If
        
    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function B_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Nondimentional b coefficient of PR-EOS representing particles finite volume
    ''B = b_cr * p / (R * T)
    On Error GoTo err_gate
    
    B_fct = cmp_b * p / (R * T)
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "clsComponent.B_fct", Err.Description

End Function

Private Function d_B_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Partial derivative of B coefficient with respect to temperature at constant pressure
    ''(dB/dT)_p = -B / T
    On Error GoTo err_gate
    
    d_B_fct = -cmp_b * p / (R * T ^ 2)
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "clsComponent.d_B_fct", Err.Description

End Function

Private Function cmp_trel(ByVal T As Double, ByVal T_ref As Double) As Double
    ''Relative temperature function
    ''T_R = T / T_ref
    On Error GoTo err_gate
    
    cmp_trel = T / T_ref
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "clsComponent.cmp_trel", Err.Description
    
End Function

Private Function cmp_prel(ByVal p As Double, ByVal p_ref As Double) As Double
    ''Relative pressure function
    ''p_R = p / p_ref
    On Error GoTo err_gate
    
    cmp_prel = p / p_ref
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "clsComponent.cmp_prel", Err.Description
    
End Function
