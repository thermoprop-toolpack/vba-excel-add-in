VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ICmpAPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.ClassModules")
''-----------------------------------------------------------------------------------------------''

Option Explicit

Public Property Get symbol() As String
End Property

Public Property Let symbol(ByVal symbol As String)
End Property

Public Property Get Tc() As Double
End Property

Public Property Get pc() As Double
End Property

Public Property Get accf() As Double
End Property

Public Property Get MM() As Double
End Property

Public Property Get T() As Double
End Property

Public Property Let T(ByVal T As Double)
End Property

Public Property Get p() As Double
End Property

Public Property Let p(ByVal p As Double)
End Property

Public Property Get v() As Double
End Property

Public Property Get rho() As Double
End Property

Public Property Get Tref() As Double
End Property

Public Property Let Tref(ByVal Tref As Double)
End Property

Public Property Get pref() As Double
End Property

Public Property Let pref(ByVal pref As Double)
End Property

Public Property Get h() As Double
End Property

Public Property Get hf() As Double
End Property

Public Property Get hsig(ByVal T As Double) As Double
End Property

Public Property Get u() As Double
End Property

Public Property Get g() As Double
End Property

Public Property Get f() As Double
End Property

Public Property Get s() As Double
End Property

Public Property Get s0() As Double
End Property

Public Property Get ssig(ByVal T As Double) As Double
End Property

Public Property Get cp() As Double
End Property

Public Property Get cpsig(ByVal T As Double) As Double
End Property

Public Property Get cv() As Double
End Property

Public Property Get cvsig(ByVal T As Double) As Double
End Property

Public Property Get x() As Double
End Property

Public Property Get Z() As Double
End Property

Public Property Get fug() As Double
End Property

Public Property Get deltaFugacityPhaseChange() As Double
End Property

Public Property Get ac() As Double
End Property

Public Property Get bc() As Double
End Property

Public Property Get k() As Double
End Property

Public Property Get alpha() As Double
End Property

Public Property Get dAlpha() As Double
End Property

Public Property Get d2Alpha() As Double
End Property

Public Sub Init(ByVal symbol As String, Optional ByVal p As Double = 0#, _
                Optional ByVal T As Double = 0#, Optional ByVal phase As String = "gas")
End Sub

