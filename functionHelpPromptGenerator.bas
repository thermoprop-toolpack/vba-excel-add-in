Attribute VB_Name = "functionHelpPromptGenerator"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.Utilities")
''-----------------------------------------------------------------------------------------------''

Option Explicit

Public Sub define_function()
    On Error GoTo err_gate
    
    Dim rg As Range, help_rg As Range
    Dim func_count As Integer, i As Integer
    Dim func_name As String, func_descr As String
    Dim func_args As Variant
    
    Set rg = ThisWorkbook.Worksheets("tbFunctionsList").Range("tbFunctionsList")
    func_count = rg.Rows.Count
    
    For i = 1 To func_count - 1
        Set help_rg = rg.Resize(1).Offset(i, 0)
        func_name = help_rg.Cells(1, 2).value
        func_args = Split(help_rg.Cells(1, 4).value, ";")
        func_descr = help_rg.Cells(1, 6).value
        
        If func_args(0) = "None" Then
            Application.MacroOptions Macro:=func_name, _
                                     Description:=func_descr

        Else
            Application.MacroOptions Macro:=func_name, _
                                     Description:=func_descr, _
                                     ArgumentDescriptions:=func_args
                            
        End If
    
        Erase func_args
        
    Next i
    
normal_exit:
    Exit Sub
    
err_gate:
    Err.Raise Err.number, "functionHelpPromptGenerator.define_function", Err.Description

End Sub

''--This is legacy function which was used in time no other solution was available. Now it stays here for debugging purposes. Function should stay private.-------
Private Function PR_EOS_help(Optional ByVal func_name As String = "PR_EOS_help") As Variant
Attribute PR_EOS_help.VB_Description = "Generates help message box for given function. Always returns 0."
Attribute PR_EOS_help.VB_ProcData.VB_Invoke_Func = " \n14"
    Dim func_table As Range
    Dim func_data As Variant, input_data As Variant, input_descr As Variant
    Dim help_rg As Range
    Dim i As Integer, no_args As Integer
    Dim input_args_str As String
    
    input_args_str = ""
    
    Set func_table = ThisWorkbook.Worksheets("tbFunctionsList").Range("tbFunctionsList")
    
    If func_name = "PR_EOS_help" Then
        MsgBox Prompt:="Please use get_func_list() to see available functions list", _
                   Buttons:=vbOKOnly + vbInformation, Title:="Help prompt"
        Exit Function
    
    End If
    
    For i = 1 To func_table.Rows.Count
        If func_table.Resize(1, 1).Offset(i, 1).value = func_name Then
            Set help_rg = func_table.Resize(1).Offset(i, 0)
            Exit For
            
        End If
    
    Next i
    
    input_data = Split(help_rg.Cells(1, 3).value, ";")
    input_descr = Split(help_rg.Cells(1, 4).value, ";")
    no_args = UBound(input_data) - LBound(input_data) + 1
    
    For i = 0 To no_args - 1
        input_args_str = input_args_str & input_data(i) & vbNewLine & vbTab & input_descr(i) & vbNewLine
    
    Next i
     
    MsgBox Prompt:="Function belongs to " & help_rg.Cells(1, 1).value & " group." & vbNewLine _
                   & "======================================" & vbNewLine _
                   & "Function name:" & vbNewLine _
                   & help_rg.Cells(1, 2).value & vbNewLine _
                   & "======================================" & vbNewLine _
                   & "Function input args:" & vbNewLine _
                   & input_args_str & vbNewLine _
                   & "======================================" & vbNewLine _
                   & "Function output args:" & vbNewLine _
                   & help_rg.Cells(1, 5).value & vbNewLine _
                   & "======================================" & vbNewLine _
                   & "Detailed description: " & vbNewLine _
                   & help_rg.Cells(1, 6).value, _
                   Buttons:=vbOKOnly + vbInformation, Title:="Help prompt"
    
End Function

