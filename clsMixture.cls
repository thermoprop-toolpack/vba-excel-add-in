VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsMixture"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.ClassModules")
''-----------------------------------------------------------------------------------------------''

Option Explicit

''--Variables------------------------------------------------------------------------------------------------------------------------
''Collection of clsComponent based on mixture composition, and mixture molar composition vector
Private mxt_cmps As New Collection, mxt_y() As Double, cmp_qt As Double

''RG properties of the mixture i.e. a, b, k from PR EOS
Private mxt_a As Double, mxt_da As Double, mxt_d2a As Double, mxt_b As Double

''Point pressure, temperature (default set to reference conditions)
Private mxt_T As Double, mxt_p As Double, mxt_Tref As Double, mxt_pref As Double

Implements IMxtCallingFunctions
Implements IMxtAPI

''--Properties-----------------------------------------------------------------------------------''
''--IMxtCallingFunctions implementation properties-----------------------------------------------''
Private Property Get IMxtCallingFunctions_MM() As Double
    IMxtCallingFunctions_MM = mxt_molarMass

End Property

Private Property Get IMxtCallingFunctions_T() As Double
    IMxtCallingFunctions_T = mxt_T

End Property

Private Property Get IMxtCallingFunctions_p() As Double
    IMxtCallingFunctions_p = mxt_p

End Property

Private Property Get IMxtCallingFunctions_Tref() As Double
    IMxtCallingFunctions_Tref = mxt_Tref

End Property

Private Property Get IMxtCallingFunctions_pref() As Double
    IMxtCallingFunctions_pref = mxt_pref

End Property

Private Property Get IMxtCallingFunctions_v() As Double
    IMxtCallingFunctions_v = mxt_specvolume(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_rho() As Double
    IMxtCallingFunctions_rho = mxt_density(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_h() As Double
    IMxtCallingFunctions_h = mxt_enth(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_u() As Double
    IMxtCallingFunctions_u = mxt_intener(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_x() As Double
    IMxtCallingFunctions_x = mxt_exergy(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_g() As Double
    IMxtCallingFunctions_g = mxt_gibbs(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_f() As Double
    IMxtCallingFunctions_f = mxt_helmholtz(mxt_p, mxt_T)
    
End Property

Private Property Get IMxtCallingFunctions_s() As Double
    IMxtCallingFunctions_s = mxt_entr(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_cp() As Double
    IMxtCallingFunctions_cp = mxt_heatcp(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_cv() As Double
    IMxtCallingFunctions_cv = mxt_heatcv(mxt_p, mxt_T)

End Property

Private Property Get IMxtCallingFunctions_Z() As Double
    IMxtCallingFunctions_Z = mxt_compr(mxt_p, mxt_T)
    
End Property

Private Sub IMxtCallingFunctions_Init(ByRef components() As String, ByRef composition() As Double, _
                                      Optional ByVal p As Double = 0, Optional ByVal T As Double = 0)
    Init components, composition, p, T

End Sub
''--IMxtAPI implementation properties------------------------------------------------------------''
Private Property Get IMxtAPI_MM() As Double
    IMxtAPI_MM = mxt_molarMass

End Property

Private Property Let IMxtAPI_T(T As Double)
    mxt_T = T

End Property

Private Property Get IMxtAPI_T() As Double
    IMxtAPI_T = mxt_T

End Property

Private Property Let IMxtAPI_p(p As Double)
    mxt_p = p

End Property

Private Property Get IMxtAPI_p() As Double
    IMxtAPI_p = mxt_p

End Property

Private Property Let IMxtAPI_Tref(Tref As Double)
    mxt_Tref = Tref
    
End Property

Private Property Get IMxtAPI_Tref() As Double
    IMxtAPI_Tref = mxt_Tref

End Property

Private Property Let IMxtAPI_pref(pref As Double)
    mxt_pref = pref
    
End Property

Private Property Get IMxtAPI_pref() As Double
    IMxtAPI_pref = mxt_pref

End Property

Private Property Get IMxtAPI_v() As Double
    IMxtAPI_v = mxt_specvolume(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_rho() As Double
    IMxtAPI_rho = mxt_density(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_h() As Double
    IMxtAPI_h = mxt_enth(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_u() As Double
    IMxtAPI_u = mxt_intener(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_x() As Double
    IMxtAPI_x = mxt_exergy(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_g() As Double
    IMxtAPI_g = mxt_gibbs(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_f() As Double
    IMxtAPI_f = mxt_helmholtz(mxt_p, mxt_T)
    
End Property

Private Property Get IMxtAPI_s() As Double
    IMxtAPI_s = mxt_entr(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_cp() As Double
    IMxtAPI_cp = mxt_heatcp(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_cv() As Double
    IMxtAPI_cv = mxt_heatcv(mxt_p, mxt_T)

End Property

Private Property Get IMxtAPI_Z() As Double
    IMxtAPI_Z = mxt_compr(mxt_p, mxt_T)
    
End Property

Private Sub IMxtAPI_Init(ByRef components() As String, ByRef composition() As Double, _
                         Optional ByVal p As Double = 0, Optional ByVal T As Double = 0)
    Init components, composition, p, T

End Sub

''--Methods--------------------------------------------------------------------------------------''
Private Sub Init(ByRef components() As String, ByRef composition() As Double, _
                Optional ByVal p As Double = 0, Optional ByVal T As Double = 0)
    On Error GoTo err_gate
    
    Dim i As Integer, j As Integer
    Dim A As Double, dA As Double, d2A As Double
    Dim oCmp As ICmpAPI
    
    cmp_qt = UBound(composition)
    ReDim mxt_y(1 To cmp_qt)
    
    ''Initialize reference conditions and base for calculation
    mxt_Tref = REF_T
    mxt_pref = REF_P
    
    If p > 0 Then
        mxt_p = p
        
    Else
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_PRESS_LEZ, _
               message:="Set pressure value (" & p & _
                        "Pa) is too low, setting mxt_p to reference value of: " & mxt_pref & "Pa.", _
               procedure:="clsMixture.Init"
        mxt_p = mxt_pref
        
    End If
        
    If T > 0 Then
        mxt_T = T
        
    Else
        logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_TEMPR_LEZ, _
               message:="Set temperature value (" & T & _
                        "K) is too low, setting mxt_T to reference value of: " & mxt_Tref & "K.", _
               procedure:="clsMixture.Init"
        mxt_T = mxt_Tref
               
    End If
    
    ''Create all components objects that were given in calling function
    For i = LBound(composition) To UBound(composition)
        Set oCmp = createCmp(components(i), p, T)
        mxt_cmps.Add oCmp, components(i)
        mxt_y(i) = composition(i)
    
    Next i
    
    ''Calculate mixture's a, da, d2a, b based on these properties from mixtures
    mxt_a = 0#
    mxt_da = 0#
    mxt_d2a = 0#
    mxt_b = 0#
    For i = 1 To cmp_qt
        For j = 1 To cmp_qt
            With mxt_cmps
                A = sqrt(.Item(i).ac * .Item(i).alpha * .Item(j).ac * .Item(j).alpha) _
                    * mxt_y(i) * mxt_y(j)
                dA = 0.5 * A * (.Item(i).dAlpha / .Item(i).alpha _
                                + .Item(j).dAlpha / .Item(j).alpha)
                d2A = 0.5 * A * (.Item(i).d2Alpha / .Item(i).alpha _
                                 + .Item(j).d2Alpha / .Item(j).alpha _
                                 - 0.5 * (.Item(i).dAlpha / .Item(i).alpha _
                                          - .Item(j).dAlpha / .Item(j).alpha) ^ 2)
                ''TODO: on a later date add binary interaction coefficients to the definition
                
            End With
            
            mxt_a = mxt_a + A
            mxt_da = mxt_da + dA
            mxt_d2a = mxt_d2a + d2A
            
        Next j
        
        mxt_b = mxt_b + mxt_y(i) * mxt_cmps.Item(i).bc
        
    Next i
    
normal_exit:
    Exit Sub
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.Init"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.Init"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Sub

Private Function mxt_molarMass() As Double
    ''Mixture molar mass calculation function
    ''MM = sumproduct(y_i, MM_i)
    On Error GoTo err_gate
    
    Dim i As Integer
    mxt_molarMass = 0#
    
    For i = 1 To cmp_qt
        mxt_molarMass = mxt_molarMass + mxt_cmps.Item(i).MM * mxt_y(i)
        
    Next i
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_molarMass"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_molarMass"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_specvolume(ByVal p As Double, ByVal T As Double) As Double
    ''Molar specific volume of the component based on PR-EOS model
    ''v = z * R * T / p
    On Error GoTo err_gate

    mxt_specvolume = mxt_compr(p, T) * R * T / p
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_specvolume"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_specvolume"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_density(ByVal p As Double, ByVal T As Double) As Double
    ''Component density based on PR-EOS model specific volume
    ''rho(p, T) = MM / v(p, T)
    On Error GoTo err_gate
    
    mxt_density = 1 / mxt_specvolume(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_density"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_density"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_enth(ByVal p As Double, ByVal T As Double) As Double
    ''Sensible enthalpy of the real gas with respect to reference conditions based on PR-EOS model
    ''h(p, T) = h_sig(T) - h_sig(Tref) + h_dept(p, T)
    ''Only sensible enthalpy, function does not include enthalpy of formation of species
    On Error GoTo err_gate
    
    mxt_enth = mxt_enth_sig(T) - mxt_enth_sig(mxt_Tref) + mxt_enth_dept(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_enth"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_enth"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_enth_sig(ByVal T As Double) As Double
    ''Enthalpy of the SIG mixture calculation
    ''h_sig = sumproduct(y_i, cmp_h_sig_i)
    On Error GoTo err_gate
    
    Dim i As Integer
    mxt_enth_sig = 0#
    
    For i = 1 To cmp_qt
        mxt_enth_sig = mxt_enth_sig + mxt_cmps.Item(i).hsig(T) * mxt_y(i)
    
    Next i
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_enth_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_enth_sig"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_enth_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Enthalpy departure calculation based on PR-EOS model
    ''h_dept = R * T * (Z - 1 - A / (B * sqrt(8)) * (1 - T * da/dT / a) * ln((Z + (sqrt(2) + 1) * B)/(Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    Dim A As Double, B As Double, Z As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    Z = mxt_compr(p, T)
    
    mxt_enth_dept = R * T * (Z - 1 - A / (sqrt(8) * B) * (1 - T * mxt_da / mxt_a) _
                    * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B)))

normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_enth_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_enth_dept"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_exergy(ByVal p As Double, ByVal T As Double) As Double
    ''Physical exergy of the mixture calculation based on PR-EOS model
    ''x(p, T) = h(p, T) - Tref * s(p, T)
    On Error GoTo err_gate
    
    mxt_exergy = mxt_enth(p, T) - mxt_Tref * mxt_entr(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_exergy"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_exergy"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_gibbs(ByVal p As Double, ByVal T As Double) As Double
    ''Gibbs free energy of the mixture calculation based on PR-EOS model
    ''g(p, T) = h(p, T) + sumproduct(y_i, h_f(T)) - T * (s(p, T) + sumproduct(y_i, s_0(T))
    
    ''TODO: Consider wether this function needs standard state entropy - in theory yes, but I'm not perfectly sure.
    On Error GoTo err_gate
    
    Dim enth_formation As Double, entr_std As Double
    Dim i As Integer
    
    enth_formation = 0#
    entr_std = 0#
    
    For i = 1 To cmp_qt
        With mxt_cmps.Item(i)
            enth_formation = enth_formation + .hsig(T_std) * mxt_y(i)
            entr_std = entr_std + .ssig(T_std) * mxt_y(i)
        
        End With
    
    Next i
    
    mxt_gibbs = mxt_enth(p, T) + enth_formation - T * (mxt_entr(p, T) + entr_std)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_gibbs"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_gibbs"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_helmholtz(ByVal p As Double, ByVal T As Double) As Double
    ''Helmhotz free energy of the mixture calculated based on PR-EOS model
    ''Unlike Gibbs free energy it is calculated relative to reference state and does not include absolute components of h_f and s_0
    ''f(p, T) = u(p, T) - T * s(p, T)
    
    On Error GoTo err_gate
    
    mxt_helmholtz = mxt_intener(p, T) - T * mxt_entr(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_helmholtz"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_helmholtz"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_intener(ByVal p As Double, ByVal T As Double) As Double
    ''Internal energy of the mixture calculation
    ''u(p, T) = h(p, T) - R * (Z(p, T) * T - Z(pref, Tref) * Tref)
    On Error GoTo err_gate
    
    mxt_intener = mxt_enth(p, T) _
                  - R * (mxt_compr(p, T) * T - mxt_compr(mxt_pref, mxt_Tref) * mxt_Tref)
                  
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_intener"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_intener"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_entr(ByVal p As Double, ByVal T As Double) As Double
    ''Total entropy of the mixture with respect to reference conditions based on PR-EOS model
    ''s(p, T) = s_sig(T) - s_sig(T_ref) + s_dept(p, T) + s_mix(p)
    On Error GoTo err_gate
    
    mxt_entr = mxt_entr_sig(T) - mxt_entr_sig(mxt_Tref) + mxt_entr_dept(p, T) + mxt_entr_mix(p)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_entr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_entr"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_entr_sig(ByVal T As Double) As Double
    ''Entropy of the SIG mixture calculation
    ''s_sig = sumproduct(y_i, cmp_s_sig_i)
    On Error GoTo err_gate
    
    Dim i As Integer
    mxt_entr_sig = 0#
    
    For i = 1 To cmp_qt
        mxt_entr_sig = mxt_entr_sig + mxt_cmps.Item(i).ssig(T) * mxt_y(i)
    
    Next i
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_entr_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_entr_sig"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_entr_mix(ByVal p As Double) As Double
    ''Entropy of the mixture with respect to pure components (mixing entropy)
    ''s_mix = sumproduct(y_i, -R * ln(p_i/p_ref))
    ''In case trace amounts of the component are available in the mixture it's mixing entropy is set to 0
    ''This is based on Dalton's Law
    On Error GoTo err_gate
    
    Dim i As Integer

    mxt_entr_mix = 0#
    
    For i = 1 To cmp_qt
        If Abs(mxt_y(i)) < 0.000001 Then
            logger log_file_path:=RNT_LOG_PATH, info_type:="Warning: " & WRN_MENTR_FTS, _
                   message:="Content of the component " & mxt_cmps.Item(i).symbol & _
                            "is too low (" & mxt_y(i) & _
                            ") to account for in mixing entropy, thus it is truncated to 0", _
                   procedure:="clsMixture.mxt_entr_mix"
                   
            mxt_entr_mix = mxt_entr_mix
        
        Else
            ''This formula is not exactly accurate for real gas (since it's based on Dalton's law) but it works
            ''More correct approach would be to use Amagat's law of partial volumes but it's more complicated calculation wise and benefits aren't that noticeable
            mxt_entr_mix = mxt_entr_mix - R * ln(mxt_y(i) * p / mxt_pref) * mxt_y(i)
            
        End If
         
    Next i
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_entr_mix"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_entr_mix"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_entr_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Entropy departure function of the mixture calculation based on PR-EOS model
    ''s_dept = R * ln(Z - B) - da/dT / (b * sqrt(8)) * ln((Z + (sqrt(2) + 1) * B)/(Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim A As Double, B As Double, Z As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    Z = mxt_compr(p, T)
    
    mxt_entr_dept = R * ln(Z - B) + mxt_da / (sqrt(8) * mxt_b) _
                    * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B))
                    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_entr_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_entr_dept"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcp(ByVal p As Double, ByVal T As Double) As Double
    ''Constant pressure specific heat of the real gas mixture based on PR-EOS model
    ''cp(p, T) = cp_sig(T) + cp_dept(p, T)
    On Error GoTo err_gate
    
    mxt_heatcp = mxt_heatcp_sig(T) + mxt_heatcp_dept(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcp"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcp"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcp_sig(ByVal T As Double) As Double
    ''Constant pressure specific heat of the ideal gas mixture calculation
    ''cp_sig = sumprouct(y_i, cmp_cp_sig_i)
    On Error GoTo err_gate
    
    Dim i As Integer
    mxt_heatcp_sig = 0#
    
    For i = 1 To cmp_qt
        mxt_heatcp_sig = mxt_heatcp_sig + mxt_cmps.Item(i).cpsig(T) * mxt_y(i)
        
    Next i
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcp_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcp_sig"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcp_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Constant pressure specific heat departure function based on PR-EOS model (to simplify equation cv_dept is calculated as well)
    ''cp_dept = cv_dept - R + R / (Z - B) - da/dT * p / (R * T * (Z ^ 2 + 2 * B * Z - B ^ 2)) * (Z + dZ/dT * T)
    On Error GoTo err_gate
    
    Dim B As Double
    Dim Z As Double, d_Z As Double
    
    B = B_fct(p, T)
    Z = mxt_compr(p, T)
    d_Z = d_mxt_compr(p, T)
    
    mxt_heatcp_dept = mxt_heatcv_dept(p, T) - R + (R / (Z - B) _
                      - mxt_da * p / (R * T) / (Z ^ 2 + 2 * B * Z - B ^ 2)) * (Z + d_Z * T)
                      
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcp_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcp_dept"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcv(ByVal p As Double, ByVal T As Double) As Double
    ''Constant volume specific heat of the real gas mixture based on PR-EOS model
    ''cv(p, T) = cv_sig(T) + cv_dept(p, T)
    On Error GoTo err_gate
    
    mxt_heatcv = mxt_heatcv_sig(T) + mxt_heatcv_dept(p, T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcv"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcv"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcv_sig(ByVal T As Double) As Double
    ''Constant volume specific heat of the ideal gas mixture calculation
    ''cv_sig = sumprouct(y_i, cmp_cv_sig_i)
    On Error GoTo err_gate
    Dim i As Integer
    mxt_heatcv_sig = 0#
    
    For i = 1 To cmp_qt
        mxt_heatcv_sig = mxt_heatcv_sig + mxt_cmps.Item(i).cvsig(T) * mxt_y(i)
    
    Next i

normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcv_sig"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcv_sig"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function mxt_heatcv_dept(ByVal p As Double, ByVal T As Double) As Double
    ''Constant volume specific heat departure function based on PR-EOS model
    ''cv_dept = T * d2a/dT2 / (b * sqrt(8)) * ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B))
    On Error GoTo err_gate
    
    Dim B As Double, Z As Double
    
    B = B_fct(p, T)
    Z = mxt_compr(p, T)
    
    mxt_heatcv_dept = (T * mxt_d2a / (mxt_b * sqrt(8))) _
                      * (ln((Z + (sqrt(2) + 1) * B) / (Z - (sqrt(2) - 1) * B)))
                      
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_heatcv_dept"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_heatcv_dept"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

''--RG properties calculations and their derivatieves used in PR-EOS model-----------------------''
''--Including Z, dZ/dT, A, dA/dT, B, dB/dT-------------------------------------------------------''
Private Function mxt_compr(ByVal p As Double, ByVal T As Double) As Double
    ''Compressibility cubic equation solution from PR-EOS model
    ''Z^3 + (B - 1) * Z ^ 2 + (-3 * B ^ 2 - 2 * B + A) * Z + (B ^ 3 + B ^ 2 - A * B) = 0
    On Error GoTo err_gate
    
    Dim A As Double, B As Double
    Dim a_3 As Double, a_2 As Double, a_1 As Double, a_0 As Double
    Dim p1 As Double, q As Double, Q1 As Double, R1 As Double, D1 As Double
    Dim theta As Double, Z(1 To 3) As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    
    a_3 = 1
    a_2 = B - 1
    a_1 = -3 * B ^ 2 - 2 * B + A
    a_0 = B ^ 3 + B ^ 2 - A * B
    
    p1 = (3 * a_1 - a_2 ^ 2) / 3
    q = (9 * a_1 * a_2 - 27 * a_0 - 2 * a_2 ^ 3) / 27
    
    Q1 = p1 / 3
    R1 = q / 2
    D1 = Q1 ^ 3 + R1 ^ 2
    
    If D1 < 0 Then
        theta = acos(R1 / sqrt(-Q1 ^ 3))
        
        Z(1) = 2 * sqrt(-Q1) * Cos(theta / 3) - a_2 / 3
        Z(2) = 2 * sqrt(-Q1) * Cos((theta + 2 * pi) / 3) - a_2 / 3
        Z(3) = 2 * sqrt(-Q1) * Cos((theta + 4 * pi) / 3) - a_2 / 3
        
        mxt_compr = max(Z)
        
    Else
        mxt_compr = cbrt(R1 + sqrt(D1)) + cbrt(R1 - sqrt(D1)) - a_2 / 3
        
    End If
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.mxt_compr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.mxt_compr"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_mxt_compr(ByVal p As Double, ByVal T As Double) As Double
    ''Compressibility equation derivative with respect to temperature at constant pressure from PR-EOS model
    ''            (B - z)(dA/dT)_p + (A - 2 * B - 3 * B ^ 2 + 6 * B * z + 2 * z - z ^ 2)(dB/dT)_p
    ''(dZ/dT)_p = -------------------------------------------------------------------------------
    ''                      3 * z ^ 2 + 2 * (B - 1) * z + (-3 * B ^ 2 - 2 * B + A)
    On Error GoTo err_gate
    
    Dim A As Double, B As Double, dA As Double, dB As Double, Z As Double
    Dim numerator As Double, denominator As Double
    
    A = A_fct(p, T)
    B = B_fct(p, T)
    dA = d_A_fct(p, T)
    dB = d_B_fct(p, T)
    Z = mxt_compr(p, T)
    
    numerator = (B - Z) * dA + (A - 2 * B - 3 * B ^ 2 + 6 * B * Z + 2 * Z - Z ^ 2) * dB
    denominator = 3 * Z ^ 2 + 2 * (B - 1) * Z + (-3 * B ^ 2 - 2 * B + A)
    
    d_mxt_compr = numerator / denominator
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.d_mxt_compr"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.d_mxt_compr"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description
    
End Function

Private Function A_fct(ByVal p As Double, T As Double) As Double
    ''Nondimentional alpha coefficient of PR-EOS
    ''A = a_cr * alpha(T) * p / (R * T) ^ 2
    On Error GoTo err_gate
    
    A_fct = mxt_a * p / (R * T) ^ 2
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.A_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.A_fct"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_A_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Partial derivative of A coefficient with respect to temperature at constant pressure
    ''(dA/dT)_p = a_c * p / (R * T) ^ 2 * ((d(alpha)/dT)_p - 2 * alpha(T) / T)
    On Error GoTo err_gate
    
    d_A_fct = p / (R * T) ^ 2 * (mxt_da - 2 * mxt_a / T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.d_A_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.d_A_fct"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function B_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Nondimentional b coefficient of PR-EOS representing particles finite volume
    ''B = b_cr * p / (R * T)
    On Error GoTo err_gate
    
    B_fct = mxt_b * p / (R * T)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.B_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.B_fct"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

Private Function d_B_fct(ByVal p As Double, ByVal T As Double) As Double
    ''Partial derivative of B coefficient with respect to temperature at constant pressure
    ''(dB/dT)_p = -B / T
    On Error GoTo err_gate
    
    d_B_fct = -mxt_b * p / (R * T ^ 2)
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.d_B_fct"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.d_B_fct"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function

''--Wrapper function for component creation------------------------------------------------------''
Private Function createCmp(ByVal name As String, Optional ByVal p As Double = 0#, _
                           Optional ByVal T As Double = 0#) As ICmpAPI
    On Error GoTo err_gate
    
    Dim oCmp As ICmpAPI
    Set oCmp = New clsComponent
    oCmp.Init name, p, T
    
    Set createCmp = oCmp
    
normal_exit:
    Exit Function
    
err_gate:
    If Err.Source = "ThermoPropPack" Then
        Err.Source = "clsMixture.createCmp"
        
    Else
        Err.Source = Err.Source & "<-" & "clsMixture.createCmp"
        
    End If

    Err.Raise Err.number, Err.Source, Err.Description

End Function
