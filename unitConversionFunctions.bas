Attribute VB_Name = "unitConversionFunctions"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.Utilities")
''-----------------------------------------------------------------------------------------------''

Option Explicit

Public Function convert_pressure(ByVal value As Double, ByVal original_unit As String, _
                                 ByVal new_unit As String, _
                                 Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_pressure.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: Pa, kPa, hPa, mbar, Mpa, bar, psi, atm, mmHg, Torr, mmH2O."
Attribute convert_pressure.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_pressure = pressure2UD(pressure2SI(value, original_unit), new_unit)
    
normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_pressure"
                       
        convert_pressure = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If
    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_pressure", Err.Description

End Function

Public Function convert_temperature(ByVal value As Double, ByVal original_unit As String, _
                                    ByVal new_unit As String, _
                                    Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_temperature.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: K, degK, �K, C, degC, �C, F, degF, �F, R, degR, �R, Ra, degRa, �Ra."
Attribute convert_temperature.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_temperature = temperature2UD(temperature2SI(value, original_unit), new_unit)
    
normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_temperature"
                       
        convert_temperature = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If
    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_temperature", Err.Description

End Function

Public Function convert_energy(ByVal value As Double, ByVal original_unit As String, _
                               ByVal new_unit As String, _
                               Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_energy.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: J, kJ, MJ, GJ, Wh, kWh, MWh, Btu, btu, MMBtu, MMbtu, tce, toe, boe, cal, kcal, Gcal."
Attribute convert_energy.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_energy = energy2UD(energy2SI(value, original_unit), new_unit)

normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_energy"
                       
        convert_energy = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If
    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_energy", Err.Description

End Function

Public Function convert_mass(ByVal value As Double, ByVal original_unit As String, _
                             ByVal new_unit As String, _
                             Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_mass.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: u, mg, g, kg, Mg, t, tonne, oz, lb, lbs, lbm, ton, long ton, UK ton, lton, imperial ton, short ton, US ton, ston."
Attribute convert_mass.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_mass = mass2UD(mass2SI(value, original_unit), new_unit)
    
normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_mass"
                       
        convert_mass = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If
    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_mass", Err.Description

End Function

Public Function convert_volume(ByVal value As Double, ByVal original_unit As String, _
                               ByVal new_unit As String, _
                               Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_volume.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: mm3, cm3, ml, dm3, l, hl, m3, in3, ft3, gal, galon, imp gal, imperial galon, UK gal, US gal, US drygal, US dgal, boo, barrel of oil, oil barel, brl."
Attribute convert_volume.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_volume = volume2UD(volume2SI(value, original_unit), new_unit)
    
normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_volume"
                       
        convert_volume = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If

    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_volume", Err.Description

End Function

Public Function convert_particles(ByVal value As Double, ByVal original_unit As String, _
                                  ByVal new_unit As String, _
                                  Optional ByVal call_from_sheet As Boolean = True) As Variant
Attribute convert_particles.VB_Description = "Converts pressure from specified, original unit into a new one. Units allowed: mol, kmol, mmol, part."
Attribute convert_particles.VB_ProcData.VB_Invoke_Func = " \n14"
    On Error GoTo err_gate
    convert_particles = particles2UD(particles2SI(value, original_unit), new_unit)
    
normal_exit:
    Exit Function
    
err_gate:
    If call_from_sheet = True Then
        logger log_file_path:=ERR_LOG_PATH, _
                       info_type:="Error code: " + Str(Err.number - vbObjectError), _
                       message:=Err.Description, _
                       procedure:=Err.Source & "<-" & "unitConversionFunctions.convert_particles"
                       
        convert_particles = "Failed to convert unit - check: " & ERR_LOG_PATH & " for details."
        
        Exit Function
        
    End If
    
    Err.Raise Err.number, Err.Source & "<-" & "unitConversionFunctions.convert_particles", _
              Err.Description

End Function

Private Function pressure2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "Pa"
            pressure2SI = ud_value
            
        Case "kPa"
            pressure2SI = 1000# * ud_value
        
        Case "mbar", "hPa"
            pressure2SI = 100# * ud_value
        
        Case "bar"
            pressure2SI = 100000# * ud_value
        
        Case "MPa"
            pressure2SI = 1000000# * ud_value
        
        Case "psi"
            pressure2SI = 6894.76 * ud_value
        
        Case "atm"
            pressure2SI = 101325 * ud_value
            
        Case "mmHg", "Torr"
            pressure2SI = 133.322421 * ud_value
           
        Case "mmH2O"
            pressure2SI = 9.80665 * ud_value
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNP, "unitConversionFunctions.pressure2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
    If pressure2SI < 0 Then
        Err.Raise vbObjectError + ERR_PRESS_LTZ, "unitConversionFunctions.pressure2SI", _
                  "Given value is not physical, SI pressure is lower than 0Pa"
    
    End If
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.pressure2SI", Err.Description

End Function

Private Function pressure2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "Pa"
            pressure2UD = ud_value
            
        Case "kPa"
            pressure2UD = ud_value / 1000#
        
        Case "mbar", "hPa"
            pressure2UD = ud_value / 100#
        
        Case "bar"
            pressure2UD = ud_value / 100000#
        
        Case "MPa"
            pressure2UD = ud_value / 1000000#
        
        Case "psi"
            pressure2UD = ud_value / 6894.76
        
        Case "atm"
            pressure2UD = ud_value / 101325
            
        Case "mmHg", "Torr"
            pressure2UD = ud_value / 133.322421
            
        Case "mmH2O"
            pressure2UD = ud_value / 9.80665
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNP, "unitConversionFunctions.pressure2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.pressure2UD", Err.Description

End Function

Private Function temperature2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "K", "degK", "�K"
            temperature2SI = ud_value
        
        Case "C", "degC", "�C"
            temperature2SI = ud_value + 273.15
        
        Case "F", "degF", "�F"
            temperature2SI = (ud_value - 32) * 5 / 9 + 273.15
        
        Case "R", "degR", "�R", "Ra", "degRa", "�Ra"
            temperature2SI = (ud_value - 491.67) * 5 / 9 + 273.15
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNT, "unitConversionFunctions.temperature2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
    If temperature2SI < 0 Then
        Err.Raise vbObjectError + ERR_TEMPR_LTZ, "unitConversionFunctions.temperature2SI", _
                  "Given value is not physical, SI temperature is lower than 0K"
    
    End If
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.temperature2SI", Err.Description

End Function

Private Function temperature2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    Select Case ud_unit
        Case "K", "degK", "�K"
            temperature2UD = ud_value
        
        Case "C", "degC", "�C"
            temperature2UD = ud_value - 273.15
        
        Case "F", "degF", "�F"
            temperature2UD = (ud_value - 273.15) * 9 / 5 + 32
        
        Case "R", "degR", "�R", "Ra", "degRa", "�Ra"
            temperature2UD = (ud_value - 273.15) * 9 / 5 + 491.67
            
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNT, "unitConversionFunctions.temperature2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.temperature2UD", Err.Description

End Function

Private Function mass2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "u"
            mass2SI = 1.6605402E-27 * ud_value
        
        Case "mg"
            mass2SI = 0.000001 * ud_value
        
        Case "g"
            mass2SI = 0.001 * ud_value
        
        Case "kg"
            mass2SI = ud_value
        
        Case "Mg", "t", "tonne"
            mass2SI = 1000# * ud_value
        
        Case "oz"
            mass2SI = 0.0283495 * ud_value
        
        Case "lb", "lbs", "lbm"
            mass2SI = 0.453592 * ud_value
        
        Case "ton", "long ton", "UK ton", "lton", "imperial ton"
            mass2SI = 1016.05 * ud_value
        
        Case "short ton", "US ton", "ston"
            mass2SI = 907.185 * ud_value
            
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNM, "unitConversionFunctions.mass2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.mass2SI", Err.Description

End Function

Private Function mass2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "u"
            mass2UD = ud_value / 1.6605402E-27
        
        Case "mg"
            mass2UD = ud_value / 0.000001
        
        Case "g"
            mass2UD = ud_value / 0.001
        
        Case "kg"
            mass2UD = ud_value
        
        Case "Mg", "t", "tonne"
            mass2UD = ud_value / 1000#
        
        Case "oz"
            mass2UD = ud_value / 0.0283495
        
        Case "lb", "lbs", "lbm"
            mass2UD = ud_value / 0.453592
        
        Case "ton", "long ton", "UK ton", "lton", "imperial ton"
            mass2UD = ud_value / 1016.05
        
        Case "short ton", "US ton", "ston"
            mass2UD = ud_value / 907.185
            
         Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNM, "unitConversionFunctions.mass2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.mass2UD", Err.Description

End Function

Private Function volume2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "mm3"
            volume2SI = 0.000000001 * ud_value
            
        Case "cm3", "ml"
            volume2SI = 0.000001 * ud_value
            
        Case "dm3", "l"
            volume2SI = 0.001 * ud_value
            
        Case "hl"
            volume2SI = 0.1 * ud_value
        
        Case "m3"
            volume2SI = ud_value
            
        Case "in3"
            volume2SI = 0.000016387 * ud_value
            
        Case "ft3"
            volume2SI = 0.0283168 * ud_value
            
        Case "gal", "galon", "imp gal", "imperial galon", "UK gal"
            volume2SI = 0.00454609 * ud_value
            
        Case "US gal"
            volume2SI = 0.003785411784 * ud_value
            
        Case "US drygal", "US dgal"
            volume2SI = 0.00440488377086 * ud_value
            
        Case "boo", "barrel of oil", "oil barrel", "brl"
            volume2SI = 0.158987 * ud_value
    
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNV, "unitConversionFunctions.volume2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.volume2SI", Err.Description

End Function

Private Function volume2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "mm3"
            volume2UD = ud_value / 0.000000001
            
        Case "cm3", "ml"
            volume2UD = ud_value / 0.000001
            
        Case "dm3", "l"
            volume2UD = ud_value / 0.001
            
        Case "hl"
            volume2UD = ud_value / 0.1
        
        Case "m3"
            volume2UD = ud_value
            
        Case "in3"
            volume2UD = ud_value / 0.000016387
            
        Case "ft3"
            volume2UD = ud_value / 0.0283168
            
        Case "gal", "galon", "imp gal", "imperial galon", "UK gal"
            volume2UD = ud_value / 0.00454609
            
        Case "US gal"
            volume2UD = ud_value / 0.003785411784
            
        Case "US drygal", "US dgal"
            volume2UD = ud_value / 0.00440488377086
            
        Case "boo", "barrel of oil", "oil barrel", "brl"
            volume2UD = ud_value / 0.158987
    
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNV, "unitConversionFunctions.volume2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.volume2UD", Err.Description

End Function

Private Function particles2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "mol"
            particles2SI = ud_value
        
        Case "kmol"
            particles2SI = ud_value * 1000#
        
        Case "mmol"
            particles2SI = ud_value * 0.001
        
        Case "part"
            particles2SI = ud_value / NA
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNN, "unitConversionFunctions.particles2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.particles2SI", Err.Description

End Function

Private Function particles2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "mol"
            particles2UD = ud_value
        
        Case "kmol"
            particles2UD = ud_value / 1000#
        
        Case "mmol"
            particles2UD = ud_value / 0.001
        
        Case "part"
            particles2UD = ud_value * NA
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNN, "unitConversionFunctions.particles2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.particles2UD", Err.Description

End Function


Private Function energy2SI(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "J"
            energy2SI = ud_value
        
        Case "kJ"
            energy2SI = 1000# * ud_value
        
        Case "MJ"
            energy2SI = 1000000# * ud_value
        
        Case "GJ"
            energy2SI = 1000000000# * ud_value
            
        Case "Wh"
            energy2SI = 3600# * ud_value
        
        Case "kWh"
            energy2SI = 3600000# * ud_value
        
        Case "MWh"
            energy2SI = 3600000000# * ud_value
        
        Case "Btu", "btu"
            energy2SI = 1055.06 * ud_value
            
        Case "MMBtu", "MMbtu"
            energy2SI = 1055060000# * ud_value
        
        Case "tce"
            energy2SI = 28653295129# * ud_value
        
        Case "toe"
            energy2SI = 45366946006# * ud_value
        
        Case "boe"
            energy2SI = 6120000105# * ud_value
        
        Case "cal"
            energy2SI = 4.184 * ud_value
        
        Case "kcal"
            energy2SI = 4184# * ud_value
            
        Case "Gcal"
            energy2SI = 4184000# * ud_value
            
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNE, "unitConversionFunctions.energy2SI", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.energy2SI", Err.Description

End Function

Private Function energy2UD(ByVal ud_value As Double, ByVal ud_unit As String) As Double
    On Error GoTo err_gate
    Select Case ud_unit
        Case "J"
            energy2UD = ud_value
        
        Case "kJ"
            energy2UD = ud_value / 1000#
        
        Case "MJ"
            energy2UD = ud_value / 1000000#
        
        Case "GJ"
            energy2UD = ud_value / 1000000000#
            
        Case "Wh"
            energy2UD = ud_value / 3600#
        
        Case "kWh"
            energy2UD = ud_value / 3600000#
        
        Case "MWh"
            energy2UD = ud_value / 3600000000#
        
        Case "Btu", "btu"
            energy2UD = ud_value / 1055.06
            
        Case "MMBtu", "MMbtu"
            energy2UD = ud_value / 1055060000#
        
        Case "tce"
            energy2UD = ud_value / 28653295129#
        
        Case "toe"
            energy2UD = ud_value / 45366946006#
        
        Case "boe"
            energy2UD = ud_value / 6120000105#
        
        Case "cal"
            energy2UD = ud_value / 4.184
        
        Case "kcal"
            energy2UD = ud_value / 4184#
            
        Case "Gcal"
            energy2UD = ud_value / 4184000#
        
        Case Else
            Err.Raise vbObjectError + ERR_UNREC_UNE, "unitConversionFunctions.energy2UD", _
                      "Unit: " & ud_unit & " is not defined in the converter"
    
    End Select
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "unitConversionFunctions.energy2UD", Err.Description

End Function

