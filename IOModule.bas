Attribute VB_Name = "IOModule"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.IOModule")
''-----------------------------------------------------------------------------------------------''

Option Explicit
Option Private Module

''TODO: Improve logger functionality to make it accessible from other VBA projects

Public Sub logger(ByVal log_file_path As String, ByVal info_type As String, _
                  ByVal message As String, ByVal procedure As String)
    ''This module can be potentially used as data dump to a file,
    ''so non-existent path error handling needs to be put in place
    Dim fso As New FileSystemObject
    Dim file As TextStream
    
    On Error GoTo err_gate
    
    ''Test for absolute path pointer for proper operation of logger
    initializationProcedures.set_working_directory
    
    Set file = fso.OpenTextFile(log_file_path, ForAppending)
    With file
        'NOTE: This "if" statement seems to be not needed here since it is carried out in initialize_log_file on startup
        If fso.GetFile(log_file_path).Size = 0 Then
            .WriteLine "Timestamp; information type; message; source"
            
        End If
        
        .WriteLine "[" + Str(Now) + "]; " + info_type + "; " + message + "; " + procedure
        .Close

    End With
    
    ''Change back the path to buff value if curdir was different than needed
    initializationProcedures.restore_working_directory
    
normal_exit:
    Exit Sub
    
err_gate:
    MsgBox "Cannot write into log since file: " & fso.GetFileName(log_file_path) & _
           " is opened in another program. Please close the file and call the function again.", _
           vbOKOnly + vbExclamation, "File opened in another program"
    
End Sub

Public Sub load_settings_file_to_dict()
    ''Load settings from thermoPropSettings.json file
    Dim fso As FileSystemObject
    Dim settings_string As String, settings_path As String
    Dim file_num As Integer
    
    Set fso = New FileSystemObject
    
    file_num = FreeFile
    
    ''Check whether EXT_PATH variable is set
    If EXT_PATH = vbNullString Then
        initializationProcedures.set_working_directory
        
    End If
    
    settings_path = EXT_PATH + "\" + STF_NAME
    
    ''Check whether SETTINGS_FILE exists
    If fso.FileExists(settings_path) Then
        Open settings_path For Input As #file_num
            settings_string = Input(LOF(file_num), file_num)
        
        Close #file_num
        
        Set SETTINGS_DICT = jsonConverter.ParseJson(settings_string)
        
    Else
        ''If file doesn't exist resets DICT object fills it with data and saves it to file
        Set SETTINGS_DICT = Nothing
        initializationProcedures.settings_dict_consistency_check
        save_settings_dict_to_file
            
    End If

End Sub

Public Sub save_settings_dict_to_file()
    ''Save settings to thermoPropSettings.json file
    Dim settings_string As String, settings_path As String
    Dim file_num As Integer
    
    file_num = FreeFile
    
    ''Check whether EXT_PATH variable is set
    If EXT_PATH = vbNullString Then
        initializationProcedures.set_working_directory
    
    End If
    
    ''File existance doesn't have to be checked since it is covered under load_settings_file_to_dict() procedure
    settings_path = EXT_PATH + "\" + STF_NAME
    
    settings_string = jsonConverter.ConvertToJson(SETTINGS_DICT)
    
    Open settings_path For Output As #file_num
        Print #file_num, settings_string
    
    Close #file_num

End Sub
