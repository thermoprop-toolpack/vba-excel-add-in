Attribute VB_Name = "errorCodes"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.InitializationAndConstants")
''-----------------------------------------------------------------------------------------------''

Option Explicit

''--Error codes----------------------------------------------------------------------------------''
Global Const ERR_FILE_OPEN As Long = 600    ''Error code for file already opened in another software

Global Const ERR_WRONG_CMP As Long = 700    ''Error code for missing component (wrong name definition)
Global Const ERR_WRONG_TMP As Long = 701    ''Error code for illdefined temperature window for component (too low or too high temp)
Global Const ERR_WRONG_PHS As Long = 702    ''Error code for missing component phase coefficients

Global Const ERR_UNREC_UNP As Long = 801    ''Error code for unrecognized pressure unit specification
Global Const ERR_UNREC_UNT As Long = 802    ''Error code for unrecognized temperature unit specification
Global Const ERR_UNREC_UNE As Long = 803    ''Error code for unrecognized energy unit specification
Global Const ERR_UNREC_UNM As Long = 804    ''Error code for unrecognized mass unit specification
Global Const ERR_UNREC_UNV As Long = 805    ''Error code for unrecognized volume unit specification
Global Const ERR_UNREC_UNN As Long = 806    ''Error code for unrecognized particle unit specification
Global Const ERR_PRESS_LTZ As Long = 811    ''Error code for pressure being lower than 0 (in Pa)
Global Const ERR_TEMPR_LTZ As Long = 812    ''Error code for temperature being lower than 0 (in K)

Global Const ERR_WRCMV_LEN As Long = 900    ''Error code for not matching vector lengths in mixture composition
Global Const ERR_WRCMP_ISN As Long = 901    ''Error code for early warning that given component is not of allowed type
Global Const ERR_WRFRC_NTN As Long = 902    ''Error code for early warning that given fraction for the component is not of allowed type
Global Const ERR_UNRCN_CTP As Long = 903    ''Error code for unrecognized conversion type name
Global Const ERR_BASEC_VNS As Long = 904    ''Error code for variable not set for base calculation global
Global Const ERR_MBMMR_DBZ As Long = 990    ''Error code for 0 value of molar mass provided to the function while mass is base of conversion (Mass Base Molar Mass Referece - Division By Zero)
Global Const ERR_VBSVR_DBZ As Long = 991    ''Error code for 0 value of specific volume provided to the function while volume is base of conversion (Volume Base Specific Volume Reference - Division By Zero)

''--Warning codes--------------------------------------------------------------------------------''
Global Const WRN_CMPFR_LTZ As Long = 1000   ''Warning code for component fraction being lower than 0
Global Const WRN_CMPFR_SUM As Long = 1001   ''Warning code for component fractions sum being other than 1

Global Const WRN_GLBVR_RST As Long = 1100   ''Warning code for global unit variables reset state due to falling out of scope error

Global Const WRN_PRESS_LEZ As Long = 1200   ''Warning code for pressure passed to the component/mixture object is <=0 (can be ignored for properties fetching functions which do not require temp/press to be set)
Global Const WRN_TEMPR_LEZ As Long = 1201   ''Warning code for temperature passed to the component/mixture object is <=0 (can be ignored for properties fetching functions which do not require temp/press to be set)

Global Const WRN_MENTR_FTS As Long = 1300   ''Warning code for component fraction in the mixture being too small for mixing entropy calculation and being set to 0
