VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UF_welcome_message 
   Caption         =   "Welcome message and introduction"
   ClientHeight    =   5415
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5760
   OleObjectBlob   =   "UF_welcome_message.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UF_welcome_message"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.UserInterphaseElements")
''-----------------------------------------------------------------------------------------------''

Option Explicit

Private Sub UserForm_Initialize()
    Me.Top = Application.Top + (Application.UsableHeight / 2) - (Me.Height / 2)
    Me.Left = Application.Left + (Application.UsableWidth / 2) - (Me.Width / 2)

    Me.Label1.Caption = "This tool was developed for calculation of real gas thermodynamic properties of the exhaust gases as well as gaseous fuel. Thermodynamic properties of pure components can also be calculated." & vbNewLine & vbNewLine _
                        & "This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; version 2 of the License." & vbNewLine & vbNewLine _
                        & "This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE. See the GNU GPL v.2 for more details." & vbNewLine & vbNewLine _
                        & "You should have received a copy of the GNU GPL along with this program; if not, please visit Free Software Foundation internet site." & vbNewLine & vbNewLine _
                        & "Copyright (C) 2021, Wladyslaw Jaroszuk" & vbNewLine _
                        & "In case you have any questions, please contact write to: thermoprop.toolpack@gmail.com" & vbNewLine & vbNewLine _
                        & "ThermoProp_Toolpack_v0.5.1.xlam, v0.5.1, May 2021"
                        
    ChB_show_again.value = Not WLC_MSG_FLAG
    
End Sub

Private Sub Button_OK_Click()
    end_proceedure

End Sub

Private Sub ChB_show_again_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 2 And KeyCode = vbKeyReturn Then
        end_proceedure
        
    ElseIf Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_welcome_message
    
    End If

End Sub

Private Sub UserForm_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If Shift = 0 And KeyCode = vbKeyReturn Then
        end_proceedure
        
    ElseIf Shift = 0 And KeyCode = vbKeyEscape Then
        Unload UF_welcome_message
    
    End If

End Sub

Private Sub end_proceedure()
    WLC_MSG_FLAG = Not ChB_show_again.value
    Unload Me

End Sub
