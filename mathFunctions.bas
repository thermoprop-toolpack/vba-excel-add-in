Attribute VB_Name = "mathFunctions"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.Utilities")
''-----------------------------------------------------------------------------------------------''

Option Explicit
Option Private Module

''Module used to translate worksheet functions to something more user friendly
''in Thermoprop Toolpack code

Public Function cbrt(ByVal number As Double) As Double
    On Error GoTo err_gate

    cbrt = WorksheetFunction.Power(number, 1 / 3)

normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.cbrt", Err.Description

End Function

Public Function sqrt(ByVal number As Double) As Double
    On Error GoTo err_gate
    
    sqrt = WorksheetFunction.Power(number, 1 / 2)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.sqrt", Err.Description

End Function

Public Function atan(ByVal number As Double) As Double
    On Error GoTo err_gate
    
    atan = Atn(number)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.atan", Err.Description
    
End Function

Public Function acos(ByVal number As Double) As Double
    On Error GoTo err_gate
    
    acos = WorksheetFunction.acos(number)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.acos", Err.Description

End Function

Public Function pi() As Double
    pi = WorksheetFunction.pi

End Function

Public Function max(arr() As Double) As Double
    On Error GoTo err_gate
    
    max = WorksheetFunction.max(arr)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.max", Err.Description

End Function

Public Function min(arr() As Double) As Double
    On Error GoTo err_gate
    
    min = WorksheetFunction.min(arr)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.mix", Err.Description

End Function

Public Function ln(ByVal number As Double) As Double
    On Error GoTo err_gate
    
    ln = Log(number)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.ln", Err.Description

End Function

Public Function round_up(ByVal num As Double, Optional ByVal dec As Integer = 0) As Long
    On Error GoTo err_gate
    
    round_up = WorksheetFunction.roundUp(num, dec)
    
normal_exit:
    Exit Function

err_gate:
    Err.Raise Err.number, "mathFunctions.round_up", Err.Description

End Function


