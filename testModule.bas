Attribute VB_Name = "testModule"
''------------------------License and contact information----------------------------------------''
'ThermoProp Toolpack, thermodynamic properties of real gases library for engineering use
'Copyright (C) 2021, Wladyslaw Jaroszuk

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; version 2 of the License.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

'In case of comments and questions please write to: thermoprop.toolpack@gmail.com
'or post an Issue on https://gitlab.com/thermoprop-toolpack/vba-excel-add-in.
''-----------------------------------------------------------------------------------------------''

''--------------------------------Rubberduck header----------------------------------------------''
'@Folder("ThermoPropPack.Utilities")
''-----------------------------------------------------------------------------------------------''

Option Explicit

''Module serving testing of new features, consider as dummy

Private Function tester_function() As Variant
    MsgBox "Model:" & THERMO_MODEL & vbNewLine & _
                "pref = " & REF_P & vbNewLine & _
                 "Tref = " & REF_T & vbNewLine & _
                 "Unit T: " & UNIT_T & vbNewLine & _
                 "Unit p: " & UNIT_P & vbNewLine & _
                 "Unit E: " & UNIT_E & vbNewLine & _
                 "Unit m: " & UNIT_M & vbNewLine & _
                 "Unit V: " & UNIT_V & vbNewLine & _
                 "Unit N: " & UNIT_N, vbOKOnly
    
End Function

Private Function reset_globals() As Variant
    THERMO_MODEL = "PR"
    REF_P = 100000#
    REF_T = 298.15
    UNIT_T = "K"
    UNIT_P = "Pa"
    UNIT_E = "J"
    UNIT_M = "kg"
    UNIT_V = "m3"
    UNIT_N = "mol"
    UNIT_BASE = "mole"
    
    tester_function
    
    Application.CalculateFull

End Function

Public Function err_tester_main() As Variant
    Dim c As Double
    
    On Error GoTo err_gate
    
    c = err_tester_1
    err_tester_main = c
    
normal_exit:
    Exit Function
    
err_gate:
    logger log_file_path:=ERR_LOG_PATH, _
           info_type:="Error", _
           message:=Err.Description & " error occured during code execution", _
           procedure:=Err.Source & "<-testModule.err_tester_main"
    'Err.Raise vbObjectError + 514, Err.Source & vbNewLine & "testModule.err_tester_main", Err.Description
    
End Function

Private Function err_tester_1() As Variant
    Dim B As Double
    
    On Error GoTo err_gate
    
    B = err_tester_2
    err_tester_1 = B
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, Err.Source & "<-" & "testModule.err_tester_1", Err.Description

End Function

Private Function err_tester_2() As Variant
    Dim A As Double
    
    On Error GoTo err_gate
    
    A = 7 / 0
    err_tester_2 = A
    
normal_exit:
    Exit Function
    
err_gate:
    Err.Raise Err.number, "testModule.err_tester_2", Err.Description

End Function
